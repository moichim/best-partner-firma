<?php
/**
 * Tiny functions, output processors and helpers 
 * used all across the theme.
 * 
 * @package bpf
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * The global excerpt length.
 */
function bpf_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'bpf_excerpt_length');

/**
 * Trim a string by words.
 * 
 * @param string $string The input text.
 * @return string String cut to the limit of words.
 */
function limit_string($string, $limit = 100) {
    # If the string is already shorter than the limit
    if( strlen($string) < $limit ) {
        return $string;
    }
    
    # Else: trim words
    $regex = "/(.{1,$limit})\b/";
    preg_match($regex, $string, $matches);
    return $matches[1];
}

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function bpf_edit_excerpt_more($more) {
    return '...<a class="arrow-right" href="' . get_the_permalink() . '">'. __("Read more",'bpf') . '</a>';
}
add_filter( 'excerpt_more', 'bpf_edit_excerpt_more' );


/**
 * Print the post categories
 */
function bpf_post_categories($post=NULL){
    charge_post($post);
    $cats = wp_get_post_categories($post->ID);
    if (gettype($cats)=="array") {
        $output = "";
        $count = count($cats);
        foreach ($cats as $cat) {
            $count--;
            $cat = get_term($cat);
            $pattern = "<a href='%s'>%s</a>";
            $output .= sprintf($pattern,get_term_link($cat->term_id),$cat->name);
            if ($count!=0) {
                $output .= ", ";
            }
        }
        return $output;
    } else {
        return false;
    }
}

/**
 * Featured image holder for the frontend
 */
function bpf_featured_holder($post=NULL){
    charge_post($post);
    if (has_post_thumbnail($post->ID) ) {
        if ( $thumb = get_post_thumbnail_id() ) {
            $image = wp_get_attachment_image_url($thumb);

            print "<div class='featured-image-holder' style='background-image:url(\"$image\")'>";
            print "</div>";
        }
    }
}

/**
 * Featured image holder for the frontend
 */
function bpf_featured_src(){

    if ( get_header_image() ) :
        $pattern = "background-image:url(%s)";
        $image = "background-image:url(".esc_url( get_header_image() ).")";
        return $image;
    endif;
    /*
    # if the query is a post
    if ( is_singular() ) {
        $post=NULL;
        charge_post($post);
        $pattern = "background-image:url(%s)";
        $size = array(1500,700);
        # disable thumbs on attachment pages
        if ( is_attachment() ) {
            $image = wp_get_attachment_image_url($post->ID,$size);
            return false;
            return sprintf($pattern,$image);
        }
        # each other displayable types has it
        if (has_post_thumbnail($post->ID) ) {
            
            if ( $thumb = get_post_thumbnail_id() ) {
                $image = wp_get_attachment_image_url($thumb,$size);
                return sprintf($pattern,$image);
            }
        }
    } # end post conditions
    */
}

/**
 * Vytiskne logo stránky
 * 
 * Pokud není obrazové logo, vytiskne název webu.
 */
function bpf_site_branding() {
    $logo_id = get_theme_mod( 'custom_logo' );
    $site_name = esc_attr( get_bloginfo('name') );
    $site_url = esc_url( home_url("/") );
    if ( $logo_id ) {
        $inner = wp_get_attachment_image( $logo_id , 'full' );
        $class = 'site-logo';
    } else {
        $inner = "<span class='site-name'>". $site_name . "</span>";
        $class = 'site-heading';
    }
    $output = "<div class='site-branding top-level $class' style='flex-grow:0'>";
    $output .= "<a class='home-link $class' href='$site_url' title='$site_name' rel='home'>";
    $output .= $inner;
    $output .= "</a>";
    $output .= "</div>";
    print $output;
    return;
}
/**
 * CMB2_loader:
 * Načte a vrátí hodnotu textového pole
 */
function bpf_text_field($post=NULL,$field) {
    // $post = charge_post($post);
    if ($field && gettype($field) == "string" ) {
        if ( $value = get_post_meta($post,$field,true) ) {
            return do_shortcode($value);
        }
    }
}
/**
 * CMB2_loader:
 * Načte a vrátí kompletní obrázek
 * 
 * @return string HTML celého elementu obrázku
 */
function bpf_image_field($post=NULL,$field) {
    // $post = charge_post($post);
    if ($field && gettype($field) == "string" ) {
        if ( $value = get_post_meta($post,$field,true) ) {
            $id = bpf_get_image_id($value);
            $image = wp_get_attachment_image($id,"large");
            return $image;
        }
    }
}
/**
 * CMB2 loader:
 * Načte a vrátí hodnotu obrázkového pole
 * 
 * @return string URL obrázku k užití v atributu src
 */
function bpf_image_field_src($post=NULL,$field) {
    charge_post($post);
    if ($field && gettype($field) == "string" ) {
        if ( $value = get_post_meta($post->ID,$field,true) ) {
            $id = bpf_get_image_id($value);
            $image = esc_url( wp_get_attachment_image_url($id,"large") );
            return $image;
        }
    }
}

/**
 * Zjistí ID attachmentu podle jeho URL adresy
 */
function bpf_get_image_id($image_url) {
    global $wpdb;
    $image_url = esc_url($image_url);
	$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}
/**
 * Získá a vykreslí nabídku na stránce osobního profilu
 */
function bpf_profile_offer($post=NULL) {
    charge_post($post);
    $data = get_post_meta($post->ID,"person_services_items", true);
    set_query_var("offer_counter",0);
    if (count($data)>0) {
        foreach ( $data as $item ) {
            set_query_var("offer",$item);
            get_template_part("template-parts/repeaters/profile","offer");
        }
    }
}
/**
 * Získá a vykreslí svědectví na stránce osobního profilu
 */
function bpf_profile_testimonials($post=NULL) {
    charge_post($post);
    $data = get_post_meta($post->ID,"person_references_items", true);
    if (count($data)>0) {
        foreach ( $data as $item ) {
            set_query_var("offer",$item);
            get_template_part("template-parts/repeaters/profile","testimonial");
        }
    }
}

/**
 * Získá a vykreslí embedded video u stránek či osobních profilů
 */
function bpf_embedded_video($post=NULL) {
    charge_post($post);
    #first load initial data
    $video["youtube"] = get_post_meta($post->ID, "video_youtube_url",true);
    # continue only if the post has any video
    if ( $video["youtube"] ) {
        # retrieve video ID from the URL
        parse_str( parse_url( $video["youtube"], PHP_URL_QUERY ), $params );
        if ( array_key_exists("v",$params) ) {

        }
        $video["vid"] = $params["v"];
        $video["button"] = get_post_meta($post->ID, "video_button_text",true);
        $video["text_after"] = get_post_meta($post->ID,"video_text",true);
        $video["video_form"] = get_post_meta($post->ID,"video_form",true);
        set_query_var("bpf_video",$video);
        get_template_part("template-parts/modules/embedded-video");
    }
}
/**
 * Získá a vrátí pole dostuných kategorií
 * 
 * @return array Pole kategorií ve formátu ID => "name"
 */
function bpf_blog_taxonomies() {
    $args = array(
        "taxonomy" => array("category"),
        "hide_empty" => false,
    );
    $terms = get_terms($args);
    $output = array();
    foreach ($terms as $term) {
        $output[$term->term_id] = $term->name;
    }
    return $output;
}

/**
 * Vykreslí blog u profilu
 */
function bpf_profile_blog($post=NULL) {
    charge_post($post);
    $allowed = get_post_meta($post->ID,"person_blog",true);
    if ($allowed) {
        $args = array(
            "post_type" => "post",
            "category_in" => $allowed,
            "posts_per_page" => 2,
            /*
            "tax_query" => array(
                "relation" => "OR",
                array(
                    "taxonomy" => "category",
                    'terms' => $allowed,
                ),
                array(
                    "taxonomy" => "blog_post",
                    'terms' => $allowed,
                ),
            ),
            */
        );
        $posts = new WP_Query($args);
        if ($posts->have_posts()) {
            print "<div class='thumbnail-holder grid-x grid-margin-x grid-margin-y'>";
            while ($posts->have_posts()) {
                $posts->the_post();

                get_template_part("template-parts/posts/content","thumbnail");
            }
            print "</div>";
            wp_reset_query();
        }
    }
}

/**
 * Získá post přiřazený k attachmentu
 * 
 * @param int|WP_Post attachment, k němuž zjišťujeme parenta
 * @return int ID nadřazeného postu
 */
function bpf_get_attachment_post_id($post=NULL) {
    charge_post($post);
    $parent = $post->post_parent;
    # pokud je post přiřazen, vrátí se ID
    if ($parent) {
        return $parent;
    }
}

/**
 * Získá post přiřazený k attachmentu
 * 
 * @param int|WP_Post attachment, k němuž zjišťujeme parenta
 * @return int ID nadřazeného postu
 */
function bpf_get_attachment_post_title($post=NULL) {
    $parent = bpf_get_attachment_post_id($post);
    # pokud je post přiřazen, vrátí se ID
    if ($parent) {
        return get_the_title($parent);
    } 
    # pokud post není přiřazen, vrátí se název wrbu
    else {
        return bloginfo("name");
    }
}
/**
 * Získá a vrátí permalink postu, k němuž je přiřazeno
 * aktuální médium
 * 
 * @param int|WP_Post attachment, k němuž zjišťujeme parenta
 * @return string URL adresa postu
 */
function bpf_get_attachment_post_permalink($post=NULL) {
    $parent = bpf_get_attachment_post_id($post);
    # pokud je post přiřazen, vrátí se ID
    if ($parent) {
        return get_post_permalink($parent);
    } 
    # pokud post není přiřazen, vrátí se název wrbu
    else {
        return get_site_url("/");
    }
}

/**
 * Získá a vrátí absolutní URL původní velikosti 
 * obrázku pro účely stahování
 */
function bpf_image_download_link($post=NULL) {
    charge_post($post);
    $url = wp_get_attachment_image_src($post->ID,"full");
    return $url[0];
}
/**
 * Získá a vrátí absolutní URL ke stažení nahraného souboru
 */
function bpf_file_download_link($post=NULL) {
    charge_post($post);
    $url = $post->guid;
    return esc_url( $url );
}
/**
 * Získá a vrátí obrázek v záhlaví stránky
 */
function bpf_heading_image($post=NULL){
    charge_post($post);
    $image = get_post_meta($post->ID,"header_image",true);
    if ($image) {
        return $image;
    } else {
        return false;
    }
}


/**
 * Vyčistí nadpis generických archivů
 */
add_filter('get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_year() ) {
        $title = get_the_date( _x( 'Y', 'yearly archives date format' ) );
    } elseif ( is_month() ) {
        $title = get_the_date( _x( 'F Y', 'monthly archives date format' ) );
    } elseif ( is_day() ) {
        $title = get_the_date( _x( 'F j, Y', 'daily archives date format' ) );
    } elseif ( is_tax( 'post_format' ) ) {
        if ( is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title' );
        }
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    } else {
        $title = __( 'Archives' );
    }
    return $title;
});


/**
 * Zjistí URL adresu výchozí rubriky v blogu
 */
function bpf_default_category_url() {
    $default = (int) get_option('default_category');
    $url = get_term_link($default);
    return $url;
}