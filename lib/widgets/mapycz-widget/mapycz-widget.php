<?php
/*
Widget Name: Mapy CZ
Description: Zobrazí mapu z Mapy.cz
Author: Best Partner, s.r.o.
Author URI: http://bestpartner.cz
*/

class Mapycz extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'mapycz',
			__('Mapy CZ', 'bpf'),
			array(
				'description' => __('A single testimony from one person.', 'bpf'),
			),
			array(
			),
			array(
				'map' => array(
					'type' => 'select',
					'label' => __('Select a map to display', 'bpf'),
					'options' => bpf_get_maps(),
				),
			),
			get_theme_file_path(__FILE__)
		);
	}
	function get_template_name($instance) {
		return 'mapycz-widget-template';
	}
	function get_style_name($instance) {
		return '';
	}
}
siteorigin_widget_register('mapycz', __FILE__, 'Mapycz');

function bpf_get_maps() {
	$output = array();
	$args = array(
		"post_type" => "mapy-cz-map",
	);
	$query = new WP_Query($args);
	if ($query->have_posts()) {
		while ($query->have_posts()) {
			$query->the_post();
			$output[get_the_ID()] = get_the_title();
		}
		return $output;
	}
	wp_reset_query();
	return false;
}