<?php $wid = "reveal-".$instance['panels_info']['widget_id']; ?>

<div class="reveal-widget" data-open="<?= esc_attr( $wid ); ?>">
    <div class="reveal-image cursor-pointer" style="background-image:url(<?= wp_get_attachment_image_url( $instance["image"], array(500,500) , false ); ?>);">
        <img src="" style="transform:scale(1.25);">
    </div>
    <div class="button-holder">
        <div class="button-lifter">
            <button class="button secondary reveal-button cursor-pointer">
                <?= wp_kses_post($instance['title']) ?>
            </button>
        </div>
    </div>
</div>
<div id="<?= $wid; ?>" class="reveal" data-reveal data-animation-in="fade-in fast">
    <h2><?= wp_kses_post( $instance["title"] ); ?></h2>
    <?= wp_kses_post( $instance["popup"] ); ?>
    <button class="close-button" data-close aria-label="Zavřít dialog" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>