<?php
/*
Widget Name: Obrázek s tlačítkem
Description: Rozkliknutelný feature, služba apod.
Author: Best Partner, s.r.o.
Author URI: http://bestpartner.cz
*/

class Reveal extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'reveal',
			__('Image with button', 'bpf'),
			array(
				'description' => __('An image icon with data in reveal.', 'bpf'),
			),
			array(
			),
			array(
				'title' => array(
					'type' => 'text',
					'label' => __('Title', 'bpf'),
					'default' => __('Service','bpf'),
				),
				'image' => array(
					'type' => 'media',
					'label' => __('Image', 'bpf'),
					'default' => '',
					'library' => 'image',
				),
				'popup' => array(
					'type' => 'tinymce',
					'rows' => 10,
					'label' => __('Popup content', 'bpf'),
					'description' => __("The text shall be displayed in the popup.", 'bpf'),
				),
			),
			get_theme_file_path(__FILE__)
		);
	}
	function get_template_name($instance) {
		return 'reveal-widget-template';
	}
	function get_style_name($instance) {
		return '';
	}
}
siteorigin_widget_register('reveal', __FILE__, 'Reveal');