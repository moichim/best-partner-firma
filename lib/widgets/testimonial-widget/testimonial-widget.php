<?php
/*
Widget Name: Svědectví
Description: Rozkliknutelný feature, služba apod.
Author: Best Partner, s.r.o.
Author URI: http://bestpartner.cz
*/

class Testimonial extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'testimonial',
			__('Testimonial', 'bpf'),
			array(
				'description' => __('A single testimony from one person.', 'bpf'),
			),
			array(
			),
			array(
				'title' => array(
					'type' => 'text',
					'label' => __('Name', 'bpf'),
					'default' => __('Name of the person','bpf'),
				),
				'company' => array(
					'type' => 'text',
					'label' => __('Company', 'bpf'),
				),
				'image' => array(
					'type' => 'media',
					'label' => __('Portrait', 'bpf'),
					'default' => '',
					'library' => 'image',
				),
				'testimony' => array(
					'type' => 'tinymce',
					'rows' => 10,
					'label' => __('Testimony', 'bpf'),
				),
			),
			get_theme_file_path(__FILE__)
		);
	}
	function get_template_name($instance) {
		return 'testimonial-widget-template';
	}
	function get_style_name($instance) {
		return '';
	}
}
siteorigin_widget_register('testimonial', __FILE__, 'Testimonial');