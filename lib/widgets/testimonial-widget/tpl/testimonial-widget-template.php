<?php
/**
 * Šablona pro vykreslení testimonials jako
 */
?>

<div class="testimonial-item" >

    <?php if ($instance["image"]): ?>
        <img src="<?php $image = wp_get_attachment_image_src($instance["image"],"large"); print esc_url($image[0]); ?>">
    <?php endif; ?>
    <div class="global-padding">
<h5><?= esc_textarea( $instance["title"] ); ?><?php if ($instance["company"]): ?>:<br><?= esc_textarea( $instance["company"] ); ?><?php endif; ?></h5>
        <p><?= wp_kses_post( $instance["testimony"] ); ?></p>
    </div>

</div>