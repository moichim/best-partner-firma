<?php
/*
Widget Name: Počítadlo
Description: Počítadlo číselné
Author: Best Partner, s.r.o.
Author URI: http://bestpartner.cz
*/

class Counter extends SiteOrigin_Widget {
	function __construct() {
		parent::__construct(
			'counter',
			__('Counter', 'bpf'),
			array(
				'description' => __('An interactive numeric counter.', 'bpf'),
			),
			array(
			),
			array(
				'number' => array(
					'type' => 'text',
					'label' => __('Number', 'bpf'),
					'default' => "100",
				),
                'suffix' => array(
					'type' => 'text',
					'label' => __('Suffix', 'bpf'),
					'default' => "",
				),

                'title' => array(
					'type' => 'text',
					'label' => __('Title', 'bpf'),
					'default' => "",
				),
			),
			get_theme_file_path(__FILE__)
		);
	}
	function get_template_name($instance) {
		return 'counter-widget-template';
	}
	function get_style_name($instance) {
		return '';
	}
}
siteorigin_widget_register('counter', __FILE__, 'Counter');