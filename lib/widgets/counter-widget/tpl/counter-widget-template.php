<?php 
$wid = "reveal-".$instance['panels_info']['widget_id']; 
$suffix = ($instance["suffix"] ? "<small>".$instance["suffix"]."</small>" : false);
?>

<div class="counter-widget" <?php if ($reveal): ?>data-open="<?= $wid; ?>" <?php endif; ?>>
    <div class="h1 counter-circle <?php if ($reveal): ?>cursor-pointer<?php endif; ?>">
        <div class="centrator">
            <span class="count"><?= $instance["number"];?></span> <?= $suffix; ?>
        </div>
    </div>
    <div class="counter-title text-center h3">
        <?= wp_kses_post($instance['title']) ?>
    </div>
</div>