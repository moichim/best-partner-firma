<?php
/**
 * Customisations for this theme
 * 
 * @package bpf
 */

 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Color replacements in the original file
 */

# The config that will be used for Branding page
# definitions and final CSS replacements.
$colors = array(
    "primary" => array(
        "label" => esc_html__("Brand primary","bpf"),
        "original" => "#9b0000",
    ), 
    "secondary" => array(
        "label" => esc_html__("Brand secondary","bpf"),
        "original" => "#fcee21",
    ),
    "third" => array(
        "label" => esc_html__("Brand third","bpf"),
        "original" => "#320033",
    ),
    "light_gray" => array(
        "label" => esc_html__("Light gray","bpf"),
        "original" => "#ffe7de",
    ),
    "medium_gray" => array(
        "label" => esc_html__("Medium gray","bpf"),
        "original" => "#cacaca",
    ),
    "dark_gray" => array(
        "label" => esc_html__("Dark gray","bpf"),
        "original" => "#8a8a8a",
    ),
    "white" => array(
        "label" => esc_html__("White","bpf"),
        "original" => "#ffffff",
    ),
    "black" => array(
        "label" => esc_html__("Black","bpf"),
        "original" => "#0a0a0a",
    ),
);

function bpf_preview_scheme() {
    global $colors;
    $output = "<div style='display:flex;width:100%;height:2em;'>";
    foreach ($colors as $key => $color) {
        $value = bpf_branding_get_option($key);
        if (!$value) {
            $value = $color["original"];
        }
        $output .= "<div style='background-color:".$value.";flex-grow:1;' title='".$color["label"]."'></div>";
    }
    $output .= "</div>";
    print $output;
}

$fonts = array(
    'roboto' => array(
        "name" => "Roboto",
        "type" => "sans-serif",
        "fallback" => "Helvetica, Arial, sans-serif",
        "system" => false,
        "titles" => true,
        "body" => true,
        "normal" => 400,
        "bold" => 900,
    ),
    'roboto_condensed' => array(
        "name" => "Roboto Condensed",
        "type" => "sans-serif",
        "fallback" => "Helvetica, Arial, sans-serif",
        "system" => false,
        "titles" => true,
        "body" => true,
        "normal" => 400,
        "bold" => 700,
    ),
    'roboto_slab' => array(
        "name" => "Roboto Slab",
        "type" => "serif",
        "fallback" => "Georgia, serif",
        "system" => false,
        "titles" => true,
        "body" => false,
        "normal" => 400,
        "bold" => 700,
    ),
    'pt_sans' => array(
        "name" => "PT Sans",
        "type" => "sans-serif",
        "fallback" => "Helvetica, Arial, sans-serif",
        "system" => false,
        "titles" => true,
        "body" => true,
        "normal" => 400,
        "bold" => 700,
    ),
    'pt_serif' => array(
        "name" => "PT Serif",
        "type" => "serif",
        "fallback" => "Georgia, serif",
        "system" => false,
        "titles" => true,
        "body" => true,
        "normal" => 400,
        "bold" => 700,
    ),
    'pt_sans_narrow' => array(
        "name" => "PT Sans Narrow",
        "type" => "sans-serif",
        "fallback" => "Helvetica, Arial, sans-serif",
        "system" => false,
        "titles" => true,
        "body" => false,
        "normal" => 400,
        "bold" => 700,
    ),
    'open_sans' => array(
        "name" => "Open Sans",
        "type" => "sans-serif",
        "fallback" => "Helvetica, Arial, sans-serif",
        "system" => false,
        "titles" => true,
        "body" => true,
        "normal" => 400,
        "bold" => 700,
    ),
    'times' => array(
        "name" => "Times New Roman",
        "type" => "serif",
        "fallback" => "serif",
        "system" => true,
        "titles" => true,
        "body" => true,
        "normal" => "normal",
        "bold" => "bold",
    ),
    'arial' => array(
        "name" => "Arial",
        "type" => "sans-serif",
        "fallback" => "Helvetica, Arial, sans-serif",
        "system" => true,
        "titles" => true,
        "body" => true,
        "normal" => "normal",
        "bold" => "bold",
    ),
    'georgia' => array(
        "name" => "Georgia",
        "type" => "serif",
        "fallback" => "serif",
        "system" => true,
        "titles" => true,
        "body" => true,
        "normal" => "normal",
        "bold" => "bold",
    ),

);

/**
 * Vypíše seznam fontů do Customizeru
 *
 * @param string Parametr s hodnotou "body" či "title"
 */
function bpf_fonts($scope="body") {
    global $fonts;
    $list = array();
    foreach ($fonts as $key => $font) {
        if ( $font[$scope]==true) {
            foreach (array("normal","bold") as $type ) {
                $key_final = $key."___".$type;
                $list[$key_final] = $font["name"] ." " .strtoupper($type);
            }
        }
    }
    return $list;
}

/**
 * Hide unwanted widgets from SiteOrigin
 */
function bpf_list_widgets_classes() {
    $widgets = array();
    $protected = array("SiteOrigin_Widget");
    foreach ( get_declared_classes() as $class ) {
        if ( is_subclass_of($class, 'WP_Widget') && !in_array($class,$protected) ) {
            $widgets[$class] = $class;
        }
    }
    return $widgets;
}


add_action( 'cmb2_admin_init', 'bpf_register_theme_options_metabox' );
 /**
  * Hook in and register a metabox to handle a theme options page and adds a menu item.
  */
 function bpf_register_theme_options_metabox() {
     /**
      * Registers options page menu item and form.
      */
     $cmb_options = new_cmb2_box( array(
         'id'           => 'bpf_branding_metabox',
         'title'        => esc_html__( 'Color Scheme', 'bpf' ),
         'object_types' => array( 'options-page' ),
         /*
          * The following parameters are specific to the options-page box
          * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
          */
         'option_key'      => 'bpf_branding', // The option key and admin menu page slug.
         'icon_url'        => 'dashicons-admin-customizer', // Menu icon. Only applicable if 'parent_slug' is left empty.
         // 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
         'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
         // 'capability'      => 'manage_options', // Cap required to view options-page.
         'position'        => 3, // Menu position. Only applicable if 'parent_slug' is left empty.
         // 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
         // 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
         'save_button'     => esc_html__( 'Use this style', 'bpf' ), // The text for the options-page save button. Defaults to 'Save'.
     ) );
     /*
      * Nastavení písem skrze selecty
      */
     $cmb_options->add_field( array(
         'name' => __( 'Headings Font', 'bpf' ),
         # 'desc' => __( 'recent value: ', 'bpf' ).myprefix_get_option("bpf-headings"),
         'id'   => 'bpf-headings',
         'type' => 'select',
         'default' => 'Default Text',
         'options' => bpf_fonts("titles"),
     ) );
     $cmb_options->add_field( array(
        'name' => __( 'Base Font', 'bpf' ),
        # 'desc' => __( 'recent value: ', 'bpf' ).myprefix_get_option("bpf-headings"),
        'id'   => 'bpf-body',
        'type' => 'select',
        'default' => 'Default Text',
        'options' => bpf_fonts("body"),
    ) );

    /**
     * Iterace barev
     */
    global $colors;

    foreach ($colors as $id => $color) {
        $cmb_options->add_field( array(
            'name'    => $color["label"],
            'id'      => $id,
            'type'    => 'colorpicker',
            'default' => $color["original"],
        ) );
    }
}


 /**
  * Wrapper function around cmb2_get_option
  * @since  0.1.0
  * @param  string $key     Options array key
  * @param  mixed  $default Optional default value
  * @return mixed           Option value
  */
 function bpf_branding_get_option( $key = '', $default = false ) {
     if ( function_exists( 'cmb2_get_option' ) ) {
         // Use cmb2_get_option as it passes through some key filters.
         return cmb2_get_option( 'bpf_branding', $key, $default );
     }
     // Fallback to get_option if CMB2 is not loaded yet.
     $opts = get_option( 'bpf_branding', $default );
     $val = $default;
     if ( 'all' == $key ) {
         $val = $opts;
     } elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
         $val = $opts[ $key ];
     }
     return $val;
 }



/**
 * Funkce generující emned font code 
 * na základě cmb2 settings
 */
function bpf_fonts_parse(){
    # inicializace proměnných
    # $headings["raw"] = get_theme_mod("bpf-headings","roboto_condensed___bold");
    $headings["raw"] = bpf_branding_get_option("bpf-headings","roboto_condensed___bold");
    $headings["family"] = bpf_fonts_get_family($headings["raw"]);
    $headings["type"] = bpf_fonts_get_type($headings["raw"]);
    # $body["raw"] = get_theme_mod("bpf-body","roboto_condensed___normal");
    $body["raw"] = bpf_branding_get_option("bpf-body","roboto_condensed___normal");
    $body["family"] = bpf_fonts_get_family($body["raw"]);
    $body["type"] = bpf_fonts_get_type($body["raw"]);
    global $fonts;
    $output = array();

    # sestavení jmen písem
    $output["headings"] = "\"".$fonts[$headings["family"]]["name"] . "\", ". $fonts[$headings["family"]]["type"].";";
    $output["body"] = "\"".$fonts[$body["family"]]["name"] . "\", ". $fonts[$body["family"]]["type"].";";
    $output["import"] = false; # výchozí hodnota bude přepsána

    # pokud jsou obě rodiny stejné
    if ($headings["family"] == $body["family"]) {
        $font = $fonts[$headings["family"]];
        # pokud je to nesystémové písmo, připravit importní řetězec
        if (!$font["system"]) {
            $output["import"] = bpf_font_parse_input($font);
        }
    }
    # pokud jsou obě rodiny jiné
    else {
        $headings_family = $fonts[$headings["family"]];
        $headings_type = $headings["type"];
        $body = $fonts[$body["family"]];
        if (!$fonts[$headings["family"]]["system"]) {
            $output["import"] .= bpf_font_parse_input($headings_family,$headings_type);
        }
        if (!$body["system"]) {
            $output["import"] .= bpf_font_parse_input($body);
        }
        
    }
    return $output;
}

/**
 * Vykreslí importní řetězec pro font 
 */
function bpf_font_parse_input($font,$type=NULL){
    # parse the font name for Google Fonts
    $family = str_replace(" ","+",$font["name"]);
    # parse styles for Google Fonts
    $styles = "";
    if ($type) {
        $styles = $font[$type];
    }
    else {
        $styles .= $font["normal"];
        if ($font["titles"]) {
            $styles .= ",".$font["normal"]."i,";
        }
        $styles .= $font["bold"];
        if ($font["titles"]) {
            $styles .= ",".$font["bold"]."i";
        }
    }
        
    $pattern = "@import url('https://fonts.googleapis.com/css?family=%s:%s&subset=latin-ext');";
    $output = sprintf($pattern,$family,$styles);
    return $output;
}
/**
 * Zjistí rodinu písma z komplexního klíče
 */
function bpf_fonts_get_family($slug){
    $slices = explode("___",$slug);
    if ($slices[0]) {
        return $slices[0];
    }
}
/**
 * Zjistí řez písma z komplexního klíče
 */
function bpf_fonts_get_type($slug){
    $slices = explode("___",$slug);
    if ($slices[1]) {
        return $slices[1];
    }
}

/**
 * Generování produkčního CSS souboru
 */
if ( !function_exists("bpf_branding_generate_css") ) {

    function bpf_branding_generate_css() {
            # Load the original CSS file
            $src = get_template_directory() . "/assets/css/app.css";
            $css = file_get_contents($src);
            #load the fonts replacement
            $fonts = bpf_fonts_parse();
            $fonts_iterator = array(
                "headings" => array(
                    "default" => "Georgia, sans-serif;",
                    "new" => $fonts["headings"],
                ),
                "body" => array(
                    "default" => "Helvetica, Arial, sans-serif;",
                    "new" => $fonts["body"],
                ),
            );
            $css = $fonts["import"] . $css;
            #iterate to replace font families
            foreach ($fonts_iterator as $font) {
                $css = str_replace($font["default"],$font["new"],$css);
            }

    
            # Ensure the $target directory exists
            $upload = wp_upload_dir();
            $target = $upload["basedir"] . "/bpf-cache";
            if ( !file_exists($target) ) {
                mkdir( $target,0777 );
            }
            chmod($target, 0777);
    
            # Perform the replacements based on $colors
            global $colors;
            foreach ( $colors as $key => $color ) {
                if ( $new = bpf_branding_get_option( $key ) ) {
                    $css = str_replace( $color["original"], esc_attr( $new ), $css );
                }
            }
    
            # Save the file
            file_put_contents( $target."/app.css", $css );
            chmod($target."/app.css", 0777);
        }
}

/**
 * Generate the CSS 
 * Each time the branding page is displayed
 */
if (is_admin()) {
    $secret = $_SERVER['REQUEST_URI'];
    wp_strip_all_tags($secret);
    sanitize_text_field($secret);
    if (strpos($secret, 'bpf_branding') !== false) {
        bpf_branding_generate_css();
    }
}


















 