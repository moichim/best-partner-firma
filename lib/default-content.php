<?php
/**
 * Výchozí obsah pro šablonu.
 * Nahraje se po aktivaci šablony.
 * 
 * @package bpf
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}



/**
 * Register a custom menu page.
 */
function bpf_register_default_content_menu_page() {
    add_menu_page(
        __( 'Aplikovat výchozí obsah', 'bpf' ),
        'Výchozí obsah',
        'manage_options',
        'bpf_default_content',
        'bpf_default_content',
        "dashicons-welcome-add-page",
        3
    );
}
add_action( 'admin_menu', 'bpf_register_default_content_menu_page' );


/**
 * Vytvoření výchozího obsahu
 */
function bpf_default_content() {
    if (isset($_POST)) {
        $router = array(
            "default_images" => "bpf_upload_default_images",
            "default_homepage" => "bpf_default_homepage",
            "default_menu" => "bpf_default_menu"
        );
        foreach($router as $key => $route) {
            if (array_key_exists($key,$_POST)) {
                $route();
            }
        }
    }

    ?>
    <div class="wrap">
        <h1>Vytvoření výchozího obsahu</h1>
        <p>Výchozí obsah vytvářejte jedině v případě, že máte <u>správně</u> a <u>kompletně</u> vyplněného <a href="<?= menu_page_url( "bpf_branding" , false );?>">Průvodce nastavením</a>. Tzn. projeďte si Průvodce nastavením odshora až dolů a nastavte vše jak je třeba.</p>
        <p>Následující úkony provádějte v uvedeném pořadí.</p>
        <h2>1. Nahrání výchozích obrázků</h2>
        <p>Šablona BestPartner obsahuje několik předdefinovaných ilustračních obrázků. Následující akcí je nahrajete do redakčního systému tak, aby byly k nalezení ve <a href="<?= admin_url("upload.php"); ?>">Knihovně médií</a> a mohli jste s nimi následně pracovat při tvorbě obsahu.</p>
        <p>Doporučujeme tuto akci spouštět jen jednou při vytváření webu. Každé spuštění této akce obrázky vloží znovu, nehledě na to, jestli v knihovně již jsou.</p>
        <form method="post">
            <input type="hidden" name="default_images" value="true">
            <?php submit_button("Nahrát výchozí obrázky");?>
        </form>
        <h2>2. Vytvoření předdefinované homepage</h2>
        <ol>
            <li>Bude vytvořena nová <a href="<?= admin_url("edit.php?post_type=page"); ?>">stránka</a>. Můžete vyplnit její název. Pokud název nevyplníte, stránka se bude pojmenována podle aktuální hodnoty nastavení <em>Název webu</em> (viz <a href="<?= menu_page_url("bpf-wizard",false); ?>">Průvodce nastavením</a>).</li>
            <li>K této stránce bude nahrán předdefinovaný obsah.</li>
            <li>Tato stránka bude nastavena jako homepage. Nastavení, homepage jsou normálně umístěné <a href="<?= admin_url("options-reading.php");?>">zde</a>.</li>
        </ol>
        <form method="post">
            <input type="hidden" name="default_homepage" value="true">
            <input type="text" name="new_title" placeholder="Název nové homepage"/>
            <?php submit_button("Vytvořit předdefinovanou homepage");?>
        </form>
        <h2>3. Vytvoření menu</h2>
        <?php 
            $menu_exists = wp_get_nav_menu_object( "Hlavní navigace" );
            if (!$menu_exists) : ?>
            <p>Tento web ještě nemá vytvořené výchozí menu. Již jste vytvořili homepage (bod 2)? Chcete výchozí homepage vytvořit menu?</p>
            <form method="post">
                <input type="hidden" name="default_menu" value="true">
                <?php submit_button("Vytvořit výchozí menu");?>
            </form>

        <?php else: ?>
            <p>Tento web již má vytvořené výchozí menu. Nastavte jej <a href="<?= admin_url("nav-menus.php?action=edit");?>">zde</a>.</p>
        <?php endif; ?>
    </div>
    <?php // konec vypisování výchozího obsahu
}


/**
 * Nahrání obrázku
 * 
 * @param $files
 */
function bpf_upload_default_images() {
    # array defining files and their names
    $files = array(
        array(
            "name" => "Obrázek 1",
            "path" => "cosy.jpg",
        ),
        array(
            "name" => "Obrázek 2",
            "path" => "meeting.jpg",
        ),
        array(
            "name" => "Obrázek 3",
            "path" => "party.jpg",
        ),
    );

    # načte absolutní uméístěné souboru v rámci OS
    $folder = get_stylesheet_directory() . "/assets/images/default/";
    
    # iterovat soubory
    foreach ($files as $file) {
        $filepath = $folder . $file["path"];
        $filename = basename($filepath);
        $upload_file = wp_upload_bits($filename, null, file_get_contents($filepath));
        if (!$upload_file['error']) {
            $wp_filetype = wp_check_filetype($filename, null );
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => $file["name"],
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attachment_id = wp_insert_attachment( $attachment, $upload_file['file']);
            if (!is_wp_error($attachment_id)) {
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
                wp_update_attachment_metadata( $attachment_id,  $attachment_data );
            }
        }

    }

    # vytiskne administrační oznámení
    $output = "<div class='notice notice-success'>";
    $output .= "<p>";
    $output .= "S obrázky je nyní možno pracovat v rámci redakčního systému. ";
    $output .= " <a href='".admin_url("upload.php")."'>";
    $output .= "Zobrazit obrázky";
    $output .= "</a>";
    $output .= "</p>";
    $output .= "</div>";
    print  $output;

}

/**
 * Create default homepage
 */
function bpf_default_homepage() {
    $args = array();
    if (isset($_POST)) {
        # zpracuje hodnoty formu a připraví titulek a slug
        if (array_key_exists("new_title",$_POST)) {
            if (!empty($_POST["new_title"])) {
                $args["title"] = sanitize_text_field($_POST["new_title"]);
                $args["slug"] = sanitize_title($_POST["new_title"]);
            } else {
                $args["title"] = get_bloginfo("name");
                $args["slug"] = sanitize_title(get_bloginfo("name"));
            }
        }
        # zjistí, jestli slug už existuje a kdyžtak vytvoří nový
        if (the_slug_exists($args["slug"])) {
            $args["slug"] .= "-".generate_random_string(20);
        }    
    }
    # includuje výchozí obsah
    include('default/homepage.php');

    # pole parametrů pro novou stránku
    $new_page = array(
	    'post_type' => 'page',
	    'post_title' => $args["title"],
	    'post_content' => "",
	    'post_status' => 'publish',
	    'post_author' => 1,
        'post_slug' => $args["slug"],
        'comment_status' => 'closed',
        'ping_status' => 'closed',
        'meta_input' => array(
            "_wp_page_template" => "page-front.php",
        ),
    );

    # vloží panels layout pokud nějaký je nahrán v externím souboru
    if (!empty($data)) {
        $new_page["meta_input"]["panels_data"] = $data;

    }
    /*
    $existing = get_post_meta(220,"panels_data",true);
    if (!empty($existing)) {    
        print var_export($existing);
    }
    */

    # vytvoří stránku
    $home = wp_insert_post($new_page);

    /** Akce, které proběhnou, pokud se podařilo vytvořit stránku */
    if (gettype($home) == "integer") {
        
        # objekt nové stránky
        $new = get_post($home);
        
        # vytiskne administrační oznámení
        $output = "<div class='notice notice-success'>";
        $output .= "<p>";
        $output .= "Byla vytvořena nová homepage nazvaná <strong>".esc_html__($new->post_title)."</strong>.";
        $output .= " <a href='".admin_url("post.php?post=").$new->ID."&action=edit'>";
        $output .= "Upravit novou homepage";
        $output .= "</a>";
        $output .= "</p>";
        $output .= "</div>";
        print $output;

        # nastaví novou stránku jako homepage 
        update_option("show_on_front","page");
        update_option("page_on_front",$new->ID);
    }

    return true;
}

// programmatically create some basic pages, and then set Home and Blog
/**
 *  setup a function to check if these pages exist
 */
function the_slug_exists($post_name) {
	global $wpdb;
	if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) {
		return true;
	} else {
		return false;
	}
}

function generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Vytvoří výchozí menu
 */
function bpf_default_menu() {
    $menu_name = 'Hlavní navigace';

    $menu_exists = wp_get_nav_menu_object( $menu_name );

    # If it doesn't exist, let's create it.
    if( !$menu_exists ) {
        $menu_id = wp_create_nav_menu($menu_name);

        # Set up default menu items
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  "O nás",
            'menu-item-url' => "#about", 
            'menu-item-status' => 'publish'));
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  "Nabídka",
            'menu-item-url' => "#offer", 
            'menu-item-status' => 'publish'));
        /*
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  "Jak to děláme",
            'menu-item-url' => "#how", 
            'menu-item-status' => 'publish'));
        */
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  "Kontakt",
            'menu-item-url' => "#contact", 
            'menu-item-status' => 'publish'));
        wp_update_nav_menu_item($menu_id, 0, array(
            'menu-item-title' =>  "Reference",
            'menu-item-url' => "#references", 
            'menu-item-status' => 'publish'));
        

        # nastaví menu jako výchozí
        $areas = array("primary","footer");
        foreach ($areas as $area) {
            $locations[$area] = $menu_id;
        }
        set_theme_mod("nav_menu_locations",$locations);

        # zobrazí noticku po úspěchu
        # vytiskne administrační oznámení
        $output = "<div class='notice notice-success'>";
        $output .= "<p>";
        $output .= "Výchozí menu bylo vytvořeno. ";
        $output .= " <a href='".admin_url("nav-menus.php?action=edit")."'>";
        $output .= "Upravte jej zde";
        $output .= "</a>";
        $output .= "</p>";
        $output .= "</div>";
        print  $output;

    }
    
}



