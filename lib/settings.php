<?php
/**
 * Průvodce nastavením šablony
 * 
 * @package bpf
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Po aktivaci šabony nasměrovat na průvodce nastavením.
 * 
 * @source https://gist.github.com/kharissulistiyo/8392698
 */
global $pagenow;

if ( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {
	wp_redirect(admin_url("themes.php?page=bpf-wizard"));
}

/**
 * Vytvoření admin menu
 */
# add_action("admin_menu","bpf_wizard_menu");

function bpf_wizard_menu() {
    add_menu_page(
        __('Settings Wizard','bpf'),
        __('Settings Wizard','bpf'),
        'edit_theme_options',
        'bpf-wizard',
        'bpf_wizard',
        'dashicons-admin-generic',
        1
     );
}

function bpf_wizard() {
    ?>
    <div class="wrap">
        <h1>Průvodce nastavením</h1>
        <h2>1. Instalace potřebných pluginů</h2>
        <h2>2. Jméno, slogan a loga</h2>
        <h2>3. Nastavení kontaktního formuláře</h2>
        <h2>4. Nastavení SiteOrigin Page Buildera</h2>
        <?php submit_button();?>
    </div>
    <?php
}





/***** */

class MySettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_theme_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_theme_page()
    {
        // This page will be under "Settings"
        add_menu_page(
            "Průvodce nastavením",
            "Průvodce nastavením",
            'edit_theme_options',
            'bpf-wizard',
            array($this,"create_admin_page"),
            'dashicons-admin-generic',
            3
         );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'my_option_name' );
        ?>
        <div class="wrap">
            <h1>Průvodce nastavením</h1>
            <p>Projeďte všechny body na této stránce tak, abyste byli s nastavením spokojení.</p>

            <!-- Sekce TGMPA -->
            <h2>1. Instalace všech potřebných pluginů</h2>
            <?php $this->check_plugins(); ?>

            <!-- Sekce základních nastavení -->
            <h2>2. Nastavení názvu webu, sloganu a loga</h2>
            <p>Zkontrolujte a vyplňte nejnutnější informace o firmě.</p>
            <table class="widefat fixed striped">
                <tr>
                    <th>Nastavení</th>
                    <th>Aktuální hodnota</th>
                    <th>Poznámka</th>
                    <th>Kde změnit</th>
                </tr>
                <tr>
                    <td class="column-columnname"><strong>Název webu</strong></td>
                    <td><?php bloginfo("name");?></td>
                    <td><em>Vyplňte jméno firmy.</em></td>
                    <td rowspan="3" style="vertical-align:middle;">
                        <a class="button button-primary" href="<?= admin_url( "options-general.php")?>">Základní nastavení</a>
                    </td>
                </tr>
                <tr>
                    <td><strong>Slogan webu</strong></td>
                    <td><?php bloginfo("description");?></td>
                    <td><em>Vyplňte slogan firmy. Pokud nemáte slogan, napište sem alespoň lokalitu, ve které působíte.</em></td>
                </tr>
                <tr>
                    <td><strong>Mail webu</strong></td>
                    <td><?php bloginfo("admin_email");?></td>
                    <td><em>Z tohoto mailu budou odcházet automaticky vygenerované upozornění administrátorům webu a také maily odeslané na základě žádostí o nové heslo.</em></td>
                </tr>
                <tr>
                    <td><strong>Logo webu</strong></td>
                    <td>
                    <?php 
                        $logo_id = get_theme_mod( 'custom_logo' );
                        if ($logo_id) {
                            print "<img style='max-width:100%;height:auto;' src='".wp_get_attachment_image_url($logo_id,"full")."'>";
                        } else {
                            print "<span class='dashicons dashicons-no'></span> Místo loga bude zobrazen název webu.";
                        }
                    ?>
                    </td>
                    <td><em>Logo je vidět nahoře vedle menu.</em></td>
                    <td rowspan="2" style="vertical-align:middle;">
                        <a class="button button-primary" href="<?= admin_url( '/customize.php?autofocus[section]=title_tagline' );?>" target="_blank">Přizpůsobení vzhledu</a>
                    </td>
                </tr>
                <tr>
                    <td><strong>Ikona webu</strong></td>
                    <td>
                    <?php 
                        $logo_id = get_site_icon_url();
                        if ($logo_id) {
                            print "<img style='width:32px;height:32px;' src='".get_site_icon_url()."'>";
                        } else {
                            print "<span class='dashicons dashicons-no'></span> Tento web nemá faviconu.";
                        }
                    ?>
                    </td>
                    <td><em>Ikona je vidět v záhlaví proflížeče.</em></td>
                </tr>
            </table>

            <!-- Sekce brandingu -->
            <h3>3. Branding</h3>
            <p>Přizpůsobte barevnost webu Vaší grafice. Branding možná nejlépe uvidíte a nastavíte, až poté, co budete mít naimportován výchozí obsah.</p>
            <table class="stripped widefat fixed">
                <tr>
                    <th>Nastavení</th>
                    <th>Aktuální hodnota</th>
                    <th>Poznámka</th>
                    <th>Kde změnit</th>
                </tr>
                <tr>
                    <td><strong>Obrázek v záhlaví</strong></td>
                    <td>
                        <?php 
                            $img = get_header_image();
                            if ($img) {
                                print "<img style='max-width:150px;height:auto;' src='".$img."'>";
                            } else {
                                print "<span class='dashicons dashicons-no'></span> Žádný obrázek v záhlaví.";
                            }
                        ?>
                    </td>
                    <td><em>Zobrazí se pod hlavním nadpisem stránky. Pokud není nahrán žádný obrázek, zobrazí se přechod v barevnosti aktuálního schematu.</em></td>
                    <td><a class="button button-primary" href="<?= admin_url( '/customize.php?autofocus[section]=header_image' );?>" target="_blank">Přizpůsobení vzhledu</a></td>
                </tr>
                <tr>
                    <td><strong>Barevné schéma</strong></td>
                    <td><?php bpf_preview_scheme();?></td>
                    <td><em>Definujte vlastní barevnou paletu webu.</em></td>
                    <td><a class="button button-primary" href="<?= menu_page_url( "bpf_branding" , false );?>">Barevné schema</a></td>
                </tr>
            </table>

            <!-- Sekce Pirate Forms -->
            <h3>4. Nastavení kontaktního formuláře</h3>
            <p>Kontaktní formulář je řešen pomocí pluginu <strong>Pirate Forms</strong>. Aby maily fungovaly správně, je nutné tento plugin nakonfigurovat.</p>
            <?php 
                $needle = 'pirate-forms';
                $active = array();
                foreach (get_option("active_plugins") as $plugin) {
                    $exploded = explode("/",$plugin);
                    array_push($active,$exploded[0]);
                }
                if (in_array($needle,$active)) {

                    $exists = get_option("pirate_forms_install");
                    if ($exists) {
                        $options = get_option("pirate_forms_settings_array");
                        $visible = array(
                            array(
                                "lab" => "Mail odesílající potvrzení",
                                "val" => $options["pirateformsopt_email"],
                                "desc" => "Odesilatelé zpráv dostanou automatické potvrzení a to bude odeslané z této adresy.",
                            ),
                            array(
                                "lab" => "Cílový mail",
                                "val" => $options["pirateformsopt_email_recipients"],
                                "desc" => "Odeslané zprávy budou zaslány na tento mail. V případě vícero adresátů maily oddělte čárkou.",
                            ),
                            array(
                                "lab" => "Maily jdou přes SMTP",
                                "val" => $options["pirateformsopt_use_smtp"] ? "Ano":"Ne",
                                "desc" => "SMTP zajistí, že maily nebudou spadat do spamu. Pro provoz formuláře však není nutné.",
                            ),

                        );

                    ?>
                    <p>Krom klíčových voleb vypsaných níže, je silně doporučeno přizpůsobit podobu kontaktního formuláře s pomocí nastavení <a href="<?= menu_page_url( "pirateforms-admin" , false );?>">zde</a>.</p>
                    <table class="stripped widefat fixed">
                    <?php foreach ($visible as $row) {?>
                        <tr>
                            <td><strong><?= $row["lab"]; ?></td>
                            <td><?= $row["val"] ?></td>
                        </tr>
                    <?php } ?>
                    </table>
                    <?php } ?>
                    <p><a class="button button-primary" href="<?= menu_page_url( "pirateforms-admin" , false );?>">Nastavení kontaktního formuláře</a></p>
                    <?php
                } else {
                    ?>
                    <p><span class='dashicons dashicons-no'></span> Pirate Forms není naninstalováno.</p><p><a class="button button-primary" href='<?= menu_page_url( "bpf-install-plugins" , false );?>'>Nainstalujte všechny potřebné pluginy</a></p>
                    <?php
                }
            ?>

            <!-- Sekce Page Builder -->
            <h3>5. Nastavení Page Buildera</h3>
            <p>Obsah <a title="Zde je přehled všech publikovaných stránek" href="<?= admin_url( "edit.php?post_type=page");?>">stránek</a> je skládán pomocí pluginu <strong>SiteOrigin Page Builder</strong>.</p>

            <h4>5.1. Doporučená nastavení</h4>
            <?php 
                $needle = 'siteorigin-panels';
                $active = array();
                foreach (get_option("active_plugins") as $plugin) {
                    $exploded = explode("/",$plugin);
                    array_push($active,$exploded[0]);
                }
                if (in_array($needle,$active)) {
                    ?>
                    <p>Některá nastavení Page Buildera nastavit na doporučené hodnoty. Ze <a href="<?= menu_page_url( "siteorigin_panels" , false );?>">všech voleb</a> zde vypisujeme jen ty, které je třeba přenastavit oproti výchozímu stavu. Zbytek nastavení použijte dle potřeby.</p>
                    <?php
                    $settings = get_option("siteorigin_panels_settings");

                    $visible = array(

                        array(
                            "lab" => "Row/Widget Bottom Margin",
                            "val" => $settings["margin-bottom"],
                            "desc" => "Dolní odsazení řádku doporučujeme nastavit na 0px, aby na sebe lícovaly podbarvené sekce.",
                            "rec" => 0,
                        ),

                        array(
                            "lab" => "Mobile Width",
                            "val" => $settings["mobile-width"],
                            "desc" => "Šířka displaye, do které se používá mobilní rozložení.",
                            "rec" => 780,
                        ),

                        array(
                            "lab" => "Display Learn",
                            "val" => $settings["display-learn"] ? "ano" : "ne",
                            "desc" => "Chcete být otrevování reklamími sděleními autora SiteOrigin Panels?",
                            "rec" => "ne",
                        ),
/*
                        array(
                            "lab" => "Copy Content",
                            "val" => $settings["copy-content"] ? "ano" : "ne",
                            "desc" => "Hodí se kopírovat obsah do těla postu.",
                            "rec" => "ano",
                        ),

                        array(
                            "lab" => "Copy Styles",
                            "val" => $settings["copy-styles"] ? "ano" : "ne",
                            "desc" => "Netřeba kopírovat styly do těla příspěvků.",
                            "rec" => "ne",
                        ),
                        */

                    );
                    ?>
                    <table class="stripped widefat">
                    <?php
                    foreach ($visible as $row) {
                        if ($row["val"] == $row["rec"]) {
                            $color = "green";
                            $icon = "yes";
                        } else {
                            $color = "red";
                            $icon = "no";
                        }

                        ?>
                        <tr>
                            <td><strong><?= $row["lab"]; ?></strong></td>
                            <td><?= $row["val"]; ?></td>
                            <td><span class="dashicons dashicons-<?= $icon; ?>" style="color:<?= $color ?>"></span> Doporučeno <?= $row["rec"];?></td>
                            <td><em><?= $row["desc"]; ?></em></td>
                        </tr>

                        <?php
                    } ?>
                    </table>
                    <p><a class="button button-primary" href="<?= menu_page_url( "siteorigin_panels" , false );?>">Obecná nastavení Page Buildera</a></p>

                    <!-- Kontrola aktivních widgetů -->
                    <h4>5.2. Aktivace nutných widgetů Page Buildera</h4>
                    <?php 
                    $needle = 'siteorigin-panels';
                    $active = array();
                    foreach (get_option("active_plugins") as $plugin) {
                        $exploded = explode("/",$plugin);
                        array_push($active,$exploded[0]);
                    }
                    if (in_array($needle,$active)) {
                        $active_widgets = get_option("siteorigin_widgets_active");
                        $obligatory = array(
                            array(
                                "slug" => "testimonial-widget",
                                "name" => "Obrázek s tlačítkem",
                            ),
                            array(
                                "slug" => "reveal-widget",
                                "name" => "Obrázek s tlačítkem",
                            ),
                            array(
                                "slug" => "counter-widget",
                                "name" => "Počítadlo",
                            ),
                            array(
                                "slug" => "mapycz-widget",
                                "name" => "Mapy CZ",
                            ),
                        );
                        $missing = array();
                        foreach ($obligatory as $plugin) {
                            if (array_key_exists($plugin["slug"],$active_widgets)) {
                                if ($active_widgets[$plugin["slug"]] != true) {
                                    array_push($missing,$plugin["name"]);
                                }
                            }
                        }
                        if (count($missing) > 0) {
                            $message = "";
                            for ($i=0;$i<count($missing);$i++) {
                                $message .= "<strong>".$missing[$i]."</strong>";
                                if ($i != count($missing)-1 ) {
                                    $message .= ", ";
                                } else {
                                    $message .= ".";
                                }
                            }
                            $intro = "<span class='dashicons dashicons-no' style='color:red;'></span>Je nutné aktivovat následující povinné widgety Page Buildera:";
                            $output = "<p>".$intro." ".$message."</p>";
                            print $output;
                            print '<p><a class="button button-primary" href="'. menu_page_url( "so-widgets-plugins" , false ).'">Aktivace widgetů Page Buildera</a></p>';

                        } else {
                            $output = "<span class='dashicons dashicons-yes' style='color:green;'></span> Všechny potřebné widgety jsou aktivní.";
                            print $output;
                        }
                        print "<h5>Poznámka k widgetům</h5>";
                        print "<p>Abste měli jednodušší práci s Page Builderem, při samotné práci se stránkami byl zjednodušen dialog přidávání nových widgetů.</p>";
                        print "<p>V seznamu dostupných widgetů se objeví jen ty z aktivovaných widgetů, které jsou opravdu základní a zároveň nutné pro zamýšlený design webu. Pokud aktivujete něco exotického, co by do zamýšleného designu nepasovalo (např. widget Features), stejně nebude možné s tím pracovat.</p>";
                        print "<p>Účelem tohoto omezení je zjednodušení práce se stránkami, ale také udržení konzistentního vzhledu webu.</p>";

                        ?>


                    <?php
                    } else {
                        ?>
                        <p><span class='dashicons dashicons-no'></span> SiteOrigin Panels není naninstalováno.</p><p><a class="button button-primary" href='<?= menu_page_url( "bpf-install-plugins" , false );?>'>Nainstalujte všechny potřebné pluginy</a></p>
                        <?php
                    }
                    
                } else {
                    print "<span class='dashicons dashicons-no' style='color:red;'></span> Page Builder není aktivován.";
                }
            ?>

        </div>
        <?php
    }

    /**
     * Check all required plugins
     */
    public function check_plugins() {
        $dependencies = array(
            array(
                'name'      => 'CMB2 Framework',
                'slug'      => 'cmb2',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'Disable comments',
                'slug'      => 'disable-comments',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'Page Builder by SiteOrigin',
                'slug'      => 'siteorigin-panels',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'SiteOrigin Widgets Bundle',
                'slug'      => 'so-widgets-bundle',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'Duplicate Post',
                'slug'      => 'duplicate-post',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'SO Page Builder Animate',
                'slug'      => 'so-page-builder-animate',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'Pirate Forms',
                'slug'      => 'pirate-forms',
                'required'  => true,
                'force_activation'   => true,
            ),
            array(
                'name'      => 'Popups',
                'slug'      => 'popups',
                'required'  => true,
                'force_activation'   => true,
            ),
        );
        /** Primární kontrola */
        $all_ok = true;
        $missing = array();
        $active = array();
        foreach (get_option("active_plugins") as $plugin) {
            $exploded = explode("/",$plugin);
            array_push($active,$exploded[0]);
        }
        /** Iterovat všechny pluginy */
        foreach ($dependencies as $plugin) {
            $path = $plugin["slug"];
            if (!in_array($path,$active)) {
                $all_ok = false;
                array_push($missing,$plugin["name"]);
            }
        }
        /** Pokud je vše ok, vrátit jen, že je to ok. */
        $output = false;
        if ($all_ok) {
            $output = "<p><span class='dashicons dashicons-yes' style='color:green;'></span> Všechny potřebné pluginy jsou nainstalované a aktivované.</p>";
            $output .= "<form method='post'>";
            $output .= "<input type='hidden' value='true' name='apply_default' />";
            $output .= get_submit_button('Provést doporučená nastavení pluginů');
            $output .= "</form>";
            if ($_POST) {
                if (array_key_exists("apply_default",$_POST)) {
                    if ($_POST["apply_default"]) {
                        $output .= "<div class='notice-success notice'>";
                        $output .= "<p>Doporučené hodnoty pluginů byly nastaveny.</p>";
                        $output .= "</div>";

                        bpf_plugins_default_values();
                    }
                }
            }
            $output .= "<p><span class='dashicons dashicons-warning'></span> <strong>Varování:</strong> Aplikování doporučených nastavení pluginů je dobré při vytváření webu. Pokud jste však již na webu něco nastavovali, aplikace výchozího nastavení může přepsat některá nastavení, která jste mezitím provedli.</p>";

        } else {
            $output = "<p><span class='dashicons dashicons-no' style='color:red;'></span> Schází tyto pluginy: ";
            for ($i = 0; $i < count($missing); $i++) {
                $output .= "<strong>".$missing[$i]."</strong>";
                if ($i != count($missing)-1 ) {
                    $output .= ", ";
                } else {
                    $output .= ".";
                }
            }
            $output .= "</p><p><a class='button button-primary' href='".menu_page_url( "bpf-install-plugins" , false )."'>Naninstalujte potřebné pluginy</a></p>";
        }
        print $output;




    } // úplný konec metody

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'my_option_group', // Option group
            'my_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'My Custom Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'my-setting-admin' // Page
        );  

        add_settings_field(
            'id_number', // ID
            'ID Number', // Title 
            array( $this, 'id_number_callback' ), // Callback
            'my-setting-admin', // Page
            'setting_section_id' // Section           
        );      

        add_settings_field(
            'title', 
            'Title', 
            array( $this, 'title_callback' ), 
            'my-setting-admin', 
            'setting_section_id'
        );      
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['id_number'] ) )
            $new_input['id_number'] = absint( $input['id_number'] );

        if( isset( $input['title'] ) )
            $new_input['title'] = sanitize_text_field( $input['title'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Pro úspěšný běh webu je třeba udělat všechny následující kroky:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function id_number_callback()
    {
        printf(
            '<input type="text" id="id_number" name="my_option_name[id_number]" value="%s" />',
            isset( $this->options['id_number'] ) ? esc_attr( $this->options['id_number']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function title_callback()
    {
        printf(
            '<input type="text" id="title" name="my_option_name[title]" value="%s" />',
            isset( $this->options['title'] ) ? esc_attr( $this->options['title']) : ''
        );
    }
}

if( is_admin() )
    $my_settings_page = new MySettingsPage();



/**
 * Aplikovat doporučené nastavení pluginů
 */
function bpf_plugins_default_values() {

    $options = array(
        "pirate_forms_settings_array" => array (
            'json' => true,
            'value' => array(
                'pirateformsopt_email' => get_option("admin_email"),
                'pirateformsopt_email_recipients' => get_option("admin_email"),
                'pirateformsopt_nonce' => 'yes',
                'pirateformsopt_confirm_email' => false,
                'pirateformsopt_copy_email' => 'yes',
                'pirateformsopt_thank_you_url' => false,
                'action' => 'save',
                'proper_nonce' => 'b761bf3608',
                'pirateformsopt_name_field' => 'req',
                'pirateformsopt_email_field' => 'req',
                'pirateformsopt_subject_field' => false,
                'pirateformsopt_message_field' => 'yes',
                'pirateformsopt_attachment_field' => false,
                'pirateformsopt_recaptcha_field' => false,
                'pirateformsopt_recaptcha_sitekey' => false,
                'pirateformsopt_recaptcha_secretkey' => false,
                'pirateformsopt_label_name' => 'Vaše jméno',
                'pirateformsopt_label_email' => 'Váš email',
                'pirateformsopt_label_subject' => 'Předmět',
                'pirateformsopt_label_message' => 'Zptáva',
                'pirateformsopt_label_submit_btn' => 'Odeslat vzkaz',
                'pirateformsopt_email_content' => '<h2>Děkujeme za Vaši zprávu</h2>
            <h3>Odesilatel</h3>
            <p>{name}</p>
            <h3>Email</h3>
            <p>{email}</p>
            <h3>Zpráva</h3>
            <p>{message}</p>
            <p>Brzy se Vám ozveme,</p>
            <p><strong>'.get_option("blogname").'</strong><br>'.get_option("siteurl").'</p>
            ',
                'pirateformsopt_label_err_name' => 'Vložte Vaše jméno',
                'pirateformsopt_label_err_email' => 'Vložte platnou emailovou adresu',
                'pirateformsopt_label_err_subject' => 'Vložte předmět',
                'pirateformsopt_label_err_no_content' => 'NApište nám zprávu!',
                'pirateformsopt_label_err_no_attachment' => 'Prosíme, připojte přílohu.',
                'pirateformsopt_label_submit' => 'Děkujeme, váš vzkaz byl úspěšně odeslán',
                'pirateformsopt_smtp_host' => false,
                'pirateformsopt_smtp_port' => false,
                'pirateformsopt_use_smtp_authentication' => false,
                'pirateformsopt_use_secure' => false,
                'pirateformsopt_smtp_username' => false,
                'pirateformsopt_smtp_password' => false,
                'pirateformsopt_store' => false,
                'pirateformsopt_use_smtp' => false,
            ),
        ),
        "siteorigin_panels_settings" => array(
            "json" => true,
            "value" => array(
                "post-types" => array("page"),
                "live-editor-quick-link" => true,
                "admin-widget-count" => false,
                "parallax-motion" => false,
                "display-teaser" => false,
                "display-learn" => false,
                "load-on-attach" => true,
                "title-html" => "<h3 class='widget-title'>{{title}}</h3>",
                "add-widget-class" => true,
                "bundled-widgets" => false,
                "recommended-widgete" => false,
                "responsive" => true,
                "tablet-layout" => true,
                "legacy-layout" => "auto",
                "tablet-width" => 1024,
                "mobile-width" => 780,
                "margin-bottom" => 0,
                "margin-bottom-last-row" => false,
                "margin-sides" => 30,
                "full-width-container" => "body",
                "copy-content" => true,
                "copy-styles" => false,
            ),
        ),
        
        "siteorigin_widgets_active" => array(
            "json" => false,
            "value" => array(
                "button" => false,
                "google-map" => false,
                "image" => true,
                "slider" => false,
                "editor" => true,
                "contact" => false,
                "cta" => false,
                "video" => false,
                "testimonial" => false,
                "icon" => false,
                "reveal-widget" => true,
                "counter-widget" => true,
                "features" => false,
                "accordion" => false,
                "hero" => false,
                "image-grid" => false,
                "layout-slider" => false,
                "social-media-button" => false,
                "simple-masonry" => false,
                "price-table" => false,
                "tabs" => false,
                "taxonomy" => false,
                "testimonial-widget" => true,
                "mapycz-widget" => true,
            ),
        ),
        "duplicate_post_types_enabled" => array(
            "json" => true,
            "value" => array(
                "poradce",
            )
        ),
        "show_on_front" => array(
            "json" => true,
            "value" => "page",
        ),
        "disable_comments_options" => array(
            "json" => true,
            "value" => array(
                "disabled_post_types" => array(
                    "post","page","attachment"
                ),
                "remove_everywhere" => true,
                "permanent" => false,
                "extra_post_types" => false,
                "db_version" => 6,
            ),
        ),

    );
    foreach ($options as $key => $value) {

        update_option($key,$value["value"]);
    }
}