<?php

function bpf_sanitize_clipsan( $value, $field_args, $field ) {

	/** Allowed tags */
	$allowed = array(
		'form' => array(
			'action' => array(),
			'method' => array(),
			'onsubmit' => array(),
			'target' => array(),
		),
		'div' => array(),
		'input' => array(
			'type' => array(),
			'name' => array(),
			'required' => array(),
			'value' => array(),
			'id' => array(),
			'placeholder' => true,
		),
		'button' => array(),
		'script' => array(),
	);
	$protocols = array(
		"http","https",
	);
    $value = wp_kses( $value, $allowed,$protocols );

    return $value;
}

function person_init() {
	register_post_type( 'poradce', array(
		'labels'            => array(
			'name'                => __( 'People', 'bpf' ),
			'singular_name'       => __( 'Person', 'bpf' ),
			'all_items'           => __( 'All people', 'bpf' ),
			'new_item'            => __( 'New person', 'bpf' ),
			'add_new'             => __( 'Add New', 'bpf' ),
			'add_new_item'        => __( 'Add New Person', 'bpf' ),
			'edit_item'           => __( 'Edit Person', 'bpf' ),
			'view_item'           => __( 'View Person', 'bpf' ),
			'search_items'        => __( 'Search people', 'bpf' ),
			'not_found'           => __( 'No people found', 'bpf' ),
			'not_found_in_trash'  => __( 'No people found in trash', 'bpf' ),
			'parent_item_colon'   => __( 'Parent person', 'bpf' ),
			'menu_name'           => __( 'People', 'bpf' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title','author' ),
		'has_archive'       => false,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-universal-access-alt',
		'show_in_rest'      => false,
		'rest_base'         => 'poradce',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		
		"capability_type" => array("person","people"),
		"map_meta_cap" => true,
	) );

}
add_action( 'init', 'person_init' );

function person_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['poradce'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Person updated. <a target="_blank" href="%s">View person</a>', 'bpf'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'bpf'),
		3 => __('Custom field deleted.', 'bpf'),
		4 => __('Person updated.', 'bpf'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Person restored to revision from %s', 'bpf'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Person published. <a href="%s">View person</a>', 'bpf'), esc_url( $permalink ) ),
		7 => __('Person saved.', 'bpf'),
		8 => sprintf( __('Person submitted. <a target="_blank" href="%s">Preview person</a>', 'bpf'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview person</a>', 'bpf'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Person draft updated. <a target="_blank" href="%s">Preview person</a>', 'bpf'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'person_updated_messages' );

/** Returns true if current user's ID is 1, else false */
function bpf_is_admin( $cmb ) {
	$user = wp_get_current_user();
	if ($user->has_cap("manage_options")) {
		return 1;
	} else { 
		return false;
	}
}

/**
 * CMB2 Definice
 */
add_action( 'cmb2_admin_init', 'bpf_person_metabox' );
function bpf_person_metabox() {
	$prefix = "person_";
	$closed = false;
	/** Metabox úvodního slidu */
	$intro = new_cmb2_box( array(
		'id' => $prefix . "intro",
		'title' => esc_html__('Introduction',"bpf"),
		'object_types' => array("poradce"),
		'closed' => $closed,
	));
	$intro->add_field( array(
		'name'       => esc_html__( 'Slogan', 'bpf' ),
		'desc'       => esc_html__( 'The callout on the intro slide.', 'bpf' ),
		'id'         => $prefix . 'call',
		'type'       => 'text',
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
	$intro->add_field( array(
		'name' => esc_html__( 'Cropped portrait', 'bpf' ),
		'desc'       => esc_html__( 'Format: PNG. This image must already be cropped off its background in Photoshop or other image processor.', 'bpf' ),
		'id'   => $prefix . 'image',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'query_args' => array(
			'type' => array(
				'image/png',
			),
		),
		'attributes'  => array(
			'required'    => 'required',
		),
	) );

	/** Metabox videa */
	$video = new_cmb2_box( array(
		'id' => $prefix . "video",
		'title' => esc_html__('Built-in video',"bpf"),
		'object_types' => array("poradce","page"),
		'closed' => $closed,
		'show_on_cb' => "bpf_is_admin",
	));
	$video->add_field( array(
		'name'       => esc_html__( 'Youtube URL', 'bpf' ),
		'description' => esc_html__( 'Insert the video\'s URL from Youtube', 'bpf' ),
		'id'         => 'video_youtube_url',
		'type'       => 'text',
	) );
	$video->add_field( array(
		'name'       => esc_html__( 'Button text', 'bpf' ),
		'id'         => 'video_button_text',
		'type'       => 'text',
	) );
	/*
	$video->add_field( array(
		'name'       => esc_html__( 'What shall follow the video', 'bpf' ),
		'id'         => 'video_post_type',
		'type'       => 'radio',
		'options'	 =>  array(
			'clipsan' => esc_html__("Clipsan form", 'bpf'),
			'text'	=> esc_html__( 'Text', 'bpf' ),
		),
	) );
	$video->add_field( array(
		'name'       => esc_html__( 'Clipsan form ID', 'bpf' ),
		'id'         => 'video_clipsan_id',
		'type'       => 'text',
	) );*/
	$video->add_field( array(
		'name'       => esc_html__( 'Text after the video ends', 'bpf' ),
		'id'         => 'video_text',
		'type'       => 'wysiwyg',
	) );
	$video->add_field( array(
		'name'       => esc_html__( 'Clipsan form', 'bpf' ),
		'description' => esc_html__('Insert Raw Form HTML from Clipsan','bpf'),
		'id'         => 'video_form',
		'type'       => 'textarea',
		"sanitization_cb" => "bpf_sanitize_clipsan",
	) );

	/** Podrobné představení */
	$presentation = new_cmb2_box( array(
		'id' => $prefix . "presentation",
		'title' => esc_html__('Detailed presentation',"bpf"),
		'object_types' => array( "poradce" ),
		'closed' => $closed,
	) );
	$presentation->add_field( array(
		'name' => esc_html__( 'Text', 'bpf' ),
		'id' => $prefix.'presentation_text',
		'type' => 'wysiwyg',
	) );
	$presentation->add_field( array(
		'name' => esc_html__( 'Full portrait', 'bpf' ),
		'id'   => $prefix . 'full_portrait',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'query_args' => array(
			'type' => array(
				'image/gif',
				'image/jpeg',
				'image/png',
			),
		),
	) );

	/** Služby */
	$offer = new_cmb2_box( array(
		'id' => $prefix . "services",
		'title' => esc_html__('Services',"bpf"),
		'object_types' => array( "poradce" ),
		'closed' => $closed,
	) );
	$offer->add_field( array(
		'name'       => esc_html__( 'Section heading', 'bpf' ),
		'id'         => $prefix.'services_introtext',
		'type'       => 'text',
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
	$services = $offer->add_field( array(
		# 'name' => esc_html__( 'Offer', 'bpf' ),
		'id'   => $prefix . 'services_items',
		'type' => 'group',
		'options' => array(
			'group_title' => esc_html__( 'Offer {#}', 'bpf' ),
			'add_button' => esc_html__( 'Add new offer' ,'bpf' ),
			'remove_button' => esc_html__( 'Remove this offer', 'bpf' ),
			'sortable' => true,
			'closed' => true,
		),
	) );
	$offer->add_group_field( $services, array(
		'name' => esc_html__( 'Name' , 'bpf' ),
		'id' => 'name',
		'type' => 'text',
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
	$offer->add_group_field( $services, array(
		'name' => esc_html__( 'Image' , 'bpf' ),
		'id' => 'image',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'query_args' => array(
			'type' => array(
				'image/gif',
				'image/jpeg',
				'image/png',
			),
		),
	) );
	$offer->add_group_field( $services, array(
		'name' => esc_html__( 'Description' , 'bpf' ),
		'id' => 'description',
		'type' => 'wysiwyg',
		'attributes'  => array(
			'required'    => 'required',
		),
	) );

	/** Separator 1 */
	$offer->add_field( array(
		'name'       => esc_html__( 'Image after', 'bpf' ),
		'id'         => $prefix.'separator_1',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'query_args' => array(
			'type' => array(
				'image/gif',
				'image/jpeg',
				'image/png',
			),
		),
		'attributes'  => array(
			'required'    => 'required',
		),
	) );

	/** Kontakty */
	$contact = new_cmb2_box( array(
		'id' => $prefix . "contacts",
		'title' => esc_html__('Contact & Blog',"bpf"),
		'object_types' => array( "poradce" ),
		'closed' => $closed,
	) );
	$contact->add_field( array(
		'name'       => esc_html__( 'Contact text', 'bpf' ),
		'id'         => $prefix.'contact_text',
		'type'       => 'wysiwyg',
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
	$contact->add_field( array(
		'name'       => esc_html__( 'Section heading', 'bpf' ),
		'id'         => $prefix.'blog',
		'type'       => 'multicheck',
		'options'    => bpf_blog_taxonomies(),
		'default' => 1,
		'required' => true,
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
	
	$contact->add_field( array(
		'name'       => esc_html__( 'Image after', 'bpf' ),
		'id'         => $prefix.'separator_2',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'query_args' => array(
			'type' => array(
				'image/gif',
				'image/jpeg',
				'image/png',
			),
		),
		'preview_size' => 'large',
	) );

	/** Reference */
	$references = new_cmb2_box( array(
		'id' => $prefix . "references",
		'title' => esc_html__('References',"bpf"),
		'object_types' => array( "poradce" ),
		'closed' => $closed,
	) );
	$references->add_field( array(
		'name'       => esc_html__( 'Section heading', 'bpf' ),
		'id'         => $prefix.'references_introtext',
		'type'       => 'text',
	) );
	$testimonial = $references->add_field( array(
		# 'name' => esc_html__( 'Offer', 'bpf' ),
		'id'   => $prefix . 'references_items',
		'type' => 'group',
		'options' => array(
			'group_title' => esc_html__( 'Testimonial {#}', 'bpf' ),
			'add_button' => esc_html__( 'Add new testimonial' ,'bpf' ),
			'remove_button' => esc_html__( 'Remove this testimonial', 'bpf' ),
			'sortable' => true,
			'closed' => true,
		),
	) );
	$references->add_group_field( $testimonial, array(
		'name' => esc_html__( 'Name' , 'bpf' ),
		'id' => 'name',
		'type' => 'text',
		'attributes'  => array(
			'placeholder' => esc_html__( 'Name' , 'bpf' ),
			'required'    => 'required',
		),
	) );
	$references->add_group_field( $testimonial, array(
		'name' => esc_html__( 'Company' , 'bpf' ),
		'id' => 'company',
		'type' => 'text',
		'attributes'  => array(
			'placeholder' => esc_html__( 'Company' , 'bpf' ),
			'required'    => 'required',
		),
	) );
	$references->add_group_field( $testimonial, array(
		'name' => esc_html__( 'Image' , 'bpf' ),
		'id' => 'image',
		'type' => 'file',
		'options' => array(
			'url' => false, // Hide the text input for the url
		),
		'query_args' => array(
			'type' => array(
				'image/gif',
				'image/jpeg',
				'image/png',
			),
		),
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
	$references->add_group_field( $testimonial, array(
		'name' => esc_html__( 'Description' , 'bpf' ),
		'id' => 'description',
		'type' => 'wysiwyg',
		'attributes'  => array(
			'required'    => 'required',
		),
	) );
}