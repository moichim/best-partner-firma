<?php
/**
 * Obecné definice platné pro všechny posty (či pro většinu)
 * 
 * @package bpf
 */

/**
 * FLEXIBLE CONTENT
 */

add_action( 'cmb2_admin_init', 'bpf_page_fields' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
function bpf_page_fields() {
    // Basic CMB2 Metabox declaration
    $page = new_cmb2_box( array(
        'id'           => 'page_heading',
        'title'        => __( 'Page heading image', 'bpf' ),
        'object_types' => array( 'page' ),
        'context'      => 'side', //  'normal', 'advanced', or 'side'
	    'priority'     => 'core',  //  'high', 'core', 'default' or 'low'
		'show_names'   => false, // Show field names on the left
    ) );

    // Sample Flexible Field
    $page->add_field( array(
		'name' => esc_html__( 'Header image', 'bpf' ),
		'id'   => 'header_image',
        'type' => 'file',
        'description' => __('Selected image shall appear only on Front Page pages.','bpf'),
		'options' => array(
            'url' => false, // Hide the text input for the url
            'add_upload_file_text' => __('Add an image','bpf'), // Change upload button text. Default: "Add or Upload File"
		),
		'query_args' => array(
			'type' => array(
				'image/gif',
				'image/jpeg',
				'image/png',
			),
		),
	) );
}