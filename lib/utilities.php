<?php
/**
 * Pomůcky Jana Jáchima
 * 
 * Původně byly samostatným pluginem.
 * 
 * @package bpf
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

if ( !function_exists("dev") ) {
    function dev( $input ) {
        if (WP_DEBUG && !empty($input) ) {
            print "<pre>";
            print_r($input);
            print "</pre>";
        }
    }
}
if ( !function_exists("check_array_field") ) {
    function check_array_field($array,$field,$value=NULL){
        if ( array_key_exists($field,$array) ) {
            if ( !empty($array[$field]) ) {
                if ($value==NULL) {
                    return $array[$field]; // key and a value exists => return value
                } else {
                    if ( $array[$field]==$value ) {
                        return $array[$field]; // key exist, value matches => return value
                    } else {
                        return false; // key exists, value does not match
                    }
                }
            }
        } else {
            return false; // key does not exist
        }
    }    
}

/**
 * Charge the post with value
 * @param &$post int|WP_post|NULL Available post input
 * @return &$post WP_Post|boolean The post object passed by reference
 */
if ( !function_exists("charge_post") ) {
    function charge_post(&$input=NULL) {
        // if no post specified
        if ($input==NULL) {
            $input = get_post();
            return;
        }
        // if the input is a object
        elseif (gettype($input) == "object") {
            // if is already a poost
            if (get_class($input) == "WP_Post") {
                return;
            } 
            // if is another class
            else {
                $input = get_post();
                return;
            }
        }
        // if specified by the post id
        elseif (gettype($input)=="integer") {
            $id = $input;
            $input = get_post($id);
            return;
        }
        // in all other cases get the main loop post
        else {
            $post = get_post();	
            if (isset($post)) {
                $input = $post;
            } else {
                return false;
            }
        }
    }
}

function bpf_generateRandomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}