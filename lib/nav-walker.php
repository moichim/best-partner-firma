<?php
/**
 * Navigation Walker Classes
 * 
 * @package bpf
 */

 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Main Class
 * Big THANK YOU to Junaid Bhura - http://www.junaidbhura.com/
 */

class Extended_subitem_nav_menu {

	// Variables
	private $_menu_items = array();

	/**
	 * Constructor
	 */
	public function __construct() {
		// Initialize plugin
		add_action( 'init', array( $this, 'init' ), 1 );
	}

	/**
	 * Initializes the plugin
	 */
	public function init() {
		// Add filters
		add_filter( 'wp_nav_menu_objects', array( $this, 'wp_nav_menu_objects' ), 10, 2 );
	}

	/**
	 * Extends the default function
	 *
	 * @param array   $sorted_menu_items
	 * @param object  $args
	 * @return array
	 */
	public function wp_nav_menu_objects( $sorted_menu_items, $args ) {
		// Add additional args
		if ( ! isset( $args->level ) )
			$args->level = 0;
		if ( ! isset( $args->child_of ) )
			$args->child_of = '';

		// Check if we need to do anything
		if ( ! $sorted_menu_items || ( $args->level == 0 && $args->child_of == '' ) )
			return $sorted_menu_items;

		// Build a tree
		$this->_menu_items = $sorted_menu_items;
		$temp_array = array();
		foreach ( $this->_menu_items as $item ) {
			$temp_array[ $item->menu_item_parent ][] = $item;
		}
		$tree = $this->build_items_tree( $temp_array, $temp_array[0] );

		// Prepare updated items
		$updated_items = $this->get_level_items( $tree, $args->level, $args->child_of );

		// Start array keys from 1
		$updated_items = array_filter( array_merge( array( 0 ), $updated_items ) );

		// Return updated items
		return $updated_items;
	}

	/**
	 * Builds a tree of menu items recursively
	 * 
	 * @param  array  $list
	 * @param  object $parent
	 * @return array
	 */
	private function build_items_tree( &$list, $parent, $level = 1 ) {
		$tree = array();
		
		foreach ( $parent as $k => $l ) {
			if ( isset( $list[ $l->ID ] ) )
				$l->children = $this->build_items_tree( $list, $list[ $l->ID ], $level + 1 );

			$l->level = $level;
			$tree[] = $l;
		}
		
		return $tree;
	}

	/**
	 * Gets items from a particular level
	 * 
	 * @param  array   $tree
	 * @param  integer $level
	 * @param  string  $child_of
	 * @return array
	 */
	private function get_level_items( $tree, $level = 1, $child_of = '' ) {
		$items = array();
		
		foreach ( $tree as $item ) {
			$child_of_flag = false;

			if ( $child_of != '' ) {
				if ( gettype( $child_of ) == 'integer' && $item->menu_item_parent != $child_of )
					$child_of_flag = true;
				elseif ( gettype( $child_of ) == 'string' && $item->menu_item_parent != $this->get_menu_id_from_title( $child_of ) )
					$child_of_flag = true;
			}
			
			if ( $item->level == $level && ! $child_of_flag ) {
				unset( $item->children );
				$items[] = $item;
			}

			if ( isset( $item->children ) && $item->children )
				$items = $items + $this->get_level_items( $item->children, $level, $child_of );
		}
		
		return $items;
	}

	/**
	 * Gets a menu ID based on the title of the item
	 * 
	 * @param  string $name
	 * @return string
	 */
	private function get_menu_id_from_title( $name = '' ) {
		foreach ( $this->_menu_items as $item ) {
			if ( $item->title == $name )
				return $item->ID;
		}

		return '';
	}

}


/**
 * Cleaner walker for wp_nav_menu()
 */
class BPF_walker extends Walker_Nav_Menu {

	/**
	 * Specify the item type to allow different walkers
	 * @var array
	 */
	var $nav_bar = '';

	function __construct( $nav_args = '' ) {

		$defaults = array(
			'item_type' => 'li',
			'in_top_bar' => false,
			'menu_type' => 'main-menu',
			'submenu_orientation'=> 'vertical'
		);
		$this->nav_bar = apply_filters( 'req_nav_args', wp_parse_args( $nav_args, $defaults ) );
	}

	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

    // Additionnal Class cleanup, as found in Roots_Nav_Walker - Roots Theme lib/nav.php
    // see http://roots.io/ and https://github.com/roots/roots
    $slug = sanitize_title($item->title);
    $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', '', $classes);
    $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

    $menu_type = $this->nav_bar['menu_type'];
	$classes[] = 'menu-item menu-item-' . $menu_type . ' menu-item-' . $slug;
	if ($depth == 0) {
		$classes[] .= " top-level";
	}
	/**
	 * Přepsat URL na absolutní všude jinde než na front page 
	 */
	if ( strpos($item->url,"#") !== false ) {
		if (!is_front_page() ) {
			$item->url = site_url()."/".$item->url;
		}
		$classes[] .= " has-magellan";
	}

    $classes = array_unique($classes);

		// Check for flyout
		$flyout_toggle = '';
		if ( $args->has_children && $this->nav_bar['item_type'] == 'li' ) {

			if ( $depth == 0 && $this->nav_bar['in_top_bar'] == false ) {

				$classes[] = 'has-flyout';
				$flyout_toggle = '<a href="#" class="flyout-toggle"><span></span></a>';

			} else if ( $this->nav_bar['in_top_bar'] == true ) {

				$classes[] = 'has-dropdown';
				$flyout_toggle = '';
			}

		}

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		if ( $depth > 0 ) {
			$output .= $indent . '<li ' . $value . $class_names .'>';
		} else {
			$output .= $indent . ( $this->nav_bar['in_top_bar'] == true ? '' : '' ) . '<' . $this->nav_bar['item_type'] . ' ' . $value . $class_names .'>';
		}

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		

		$item_output  = $args->before;
		$item_output .= '<a '. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $flyout_toggle; // Add possible flyout toggle
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {

		if ( $depth > 0 ) {
			$output .= "</li>\n";
		} else {
			$output .= "</" . $this->nav_bar['item_type'] . ">\n";
		}
	}

	function start_lvl( &$output, $depth = 0, $args = array() ) {

		if ( $depth == 0 && $this->nav_bar['item_type'] == 'li' ) {
			if ($this->nav_bar['submenu_orientation'] == 'vertical'){
				$indent = str_repeat("\t", 1);
				$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested vertical\">\n" : "\n$indent<ul class=\"flyout\">\n";
			} else {
				$indent = str_repeat("\t", 1);
				$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested\">\n" : "\n$indent<ul class=\"flyout\">\n";
			}
    	} else {
			if ($this->nav_bar['submenu_orientation'] == 'vertical'){
				$indent = str_repeat("\t", $depth);
				$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested vertical\">\n" : "\n$indent<ul class=\"level-$depth\">\n";
			} else {
				$indent = str_repeat("\t", $depth);
				$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested\">\n" : "\n$indent<ul class=\"level-$depth\">\n";
			}
		}
  	}
}

/**
 * Cleaner walker for wp_nav_menu()
 */
class WPF_offcnavas_walker extends Walker_Nav_Menu {
	
		/**
		 * Specify the item type to allow different walkers
		 * @var array
		 */
		var $nav_bar = '';
	
		function __construct( $nav_args = '' ) {
	
			$defaults = array(
				'item_type' => 'li',
				'in_top_bar' => false,
				'menu_type' => 'main-menu',
				'submenu_orientation'=> 'vertical'
			);
			$this->nav_bar = apply_filters( 'req_nav_args', wp_parse_args( $nav_args, $defaults ) );
		}
	
		function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
	
			$id_field = $this->db_fields['id'];
			if ( is_object( $args[0] ) ) {
				$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
			}
			return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
		}
	
		function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
	
			global $wp_query;
			$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
	
			$class_names = $value = '';
	
			$classes = empty( $item->classes ) ? array() : (array) $item->classes;
			$classes[] = 'menu-item-' . $item->ID;
	
		// Additionnal Class cleanup, as found in Roots_Nav_Walker - Roots Theme lib/nav.php
		// see http://roots.io/ and https://github.com/roots/roots
		$slug = sanitize_title($item->title);
		$classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', '', $classes);
		$classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);
	
		$menu_type = $this->nav_bar['menu_type'];
		$classes[] = 'menu-item menu-item-' . $menu_type . ' menu-item-' . $slug;
		
		$classes = array_unique($classes);
			// Check for flyout
			$flyout_toggle = '';
			if ( $args->has_children && $this->nav_bar['item_type'] == 'li' ) {
	
				if ( $depth == 0 && $this->nav_bar['in_top_bar'] == false ) {
	
					$classes[] = 'has-flyout';
					$flyout_toggle = '<a href="#" class="flyout-toggle"><span></span></a>';
	
				} else if ( $this->nav_bar['in_top_bar'] == true ) {
	
					$classes[] = 'has-dropdown';
					$flyout_toggle = '';
				}
	
			}
	
			$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
			$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
	
			if ( $depth > 0 ) {
				$output .= $indent . '<li ' . $value . $class_names .'>';
			} else {
				$output .= $indent . ( $this->nav_bar['in_top_bar'] == true ? '' : '' ) . '<' . $this->nav_bar['item_type'] . ' ' . $value . $class_names .'>';
			}
	
			$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
			$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
			$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
			$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	
			$item_output  = $args->before;
			$item_output .= '<a '. $attributes .'>';
			$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
			$item_output .= '</a>';
			$item_output .= $flyout_toggle; // Add possible flyout toggle
			$item_output .= $args->after;
	
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
	
		function end_el( &$output, $item, $depth = 0, $args = array() ) {
	
			if ( $depth > 0 ) {
				$output .= "</li>\n";
			} else {
				$output .= "</" . $this->nav_bar['item_type'] . ">\n";
			}
		}
	
		function start_lvl( &$output, $depth = 0, $args = array() ) {
	
			if ( $depth == 0 && $this->nav_bar['item_type'] == 'li' ) {
				if ($this->nav_bar['submenu_orientation'] == 'vertical'){
					$indent = str_repeat("\t", 1);
					$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested vertical\">\n" : "\n$indent<ul class=\"flyout\">\n";
				} else {
					$indent = str_repeat("\t", 1);
					$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested\">\n" : "\n$indent<ul class=\"flyout\">\n";
				}
			} else {
				if ($this->nav_bar['submenu_orientation'] == 'vertical'){
					$indent = str_repeat("\t", $depth);
					$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested vertical\">\n" : "\n$indent<ul class=\"level-$depth\">\n";
				} else {
					$indent = str_repeat("\t", $depth);
					$output .= $this->nav_bar['in_top_bar'] == true ? "\n$indent<ul class=\"menu nested\">\n" : "\n$indent<ul class=\"level-$depth\">\n";
				}
			}
		  }
	}
