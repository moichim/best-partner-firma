<?php
/**
 * Defaultní homepage
 * 
 * @package bpf
 */

$data = array (
    'widgets' => 
    array (
      0 => 
      array (
        'number' => '100',
        'suffix' => '',
        'title' => 'klientů',
        '_sow_form_id' => '5a9aeabea0aff',
        '_sow_form_timestamp' => '1520102089795',
        'panels_info' => 
        array (
          'class' => 'Counter',
          'raw' => false,
          'grid' => 0,
          'cell' => 0,
          'id' => 0,
          'widget_id' => '2eef29eb-c8fc-4de9-9359-f05a6407ef65',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'bounceInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      1 => 
      array (
        'number' => '2000',
        'suffix' => '',
        'title' => 'klientů',
        '_sow_form_id' => '5a9aead454a60',
        '_sow_form_timestamp' => '1524085305554',
        'panels_info' => 
        array (
          'class' => 'Counter',
          'raw' => false,
          'grid' => 0,
          'cell' => 1,
          'id' => 1,
          'widget_id' => '08336258-a10e-4b4c-a8dd-b687f5e45db7',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'bounceInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      2 => 
      array (
        'number' => '25',
        'suffix' => 'tis.',
        'title' => 'uzavřených smluv',
        '_sow_form_id' => '5a9aead7097c7',
        '_sow_form_timestamp' => '1520103121477',
        'panels_info' => 
        array (
          'class' => 'Counter',
          'raw' => false,
          'grid' => 0,
          'cell' => 2,
          'id' => 2,
          'widget_id' => 'ec841d1c-800c-4590-af08-424ccad0f723',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'bounceInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      3 => 
      array (
        'number' => '25',
        'suffix' => 'tis.',
        'title' => 'uzavřených smluv',
        '_sow_form_id' => '14683913305adfb6f1d66aa748013486',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Counter',
          'raw' => false,
          'grid' => 0,
          'cell' => 3,
          'id' => 3,
          'widget_id' => '5a2460c9-026c-4f62-914a-aadfc862ac2d',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'bounceInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      4 => 
      array (
        'title' => '',
        'text' => '<h2 style="text-align: center;">Naše nabídka</h2>',
        'text_selected_editor' => 'html',
        'autop' => true,
        '_sow_form_id' => '5a9b4d23be15b',
        '_sow_form_timestamp' => '1520127284888',
        'panels_info' => 
        array (
          'class' => 'SiteOrigin_Widget_Editor_Widget',
          'raw' => false,
          'grid' => 2,
          'cell' => 0,
          'id' => 4,
          'widget_id' => '11475425-d0a3-4ae5-aeb8-6456c400ded5',
          'style' => 
          array (
            'padding' => '30px 30px 30px 30px',
            'mobile_padding' => '15px 15px 15px 15px',
            'background_display' => 'tile',
            'animation_type' => 'bounceInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      5 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '5a9ae5b04baa1',
        '_sow_form_timestamp' => '1520100806632',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 3,
          'cell' => 0,
          'id' => 5,
          'widget_id' => 'edfed1a3-469b-4178-ae92-2958fbbcbdad',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      6 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '5a9aec6bbac6f',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 3,
          'cell' => 1,
          'id' => 6,
          'widget_id' => '9c87cbe4-c44f-4f5c-8604-8fd9321df036',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      7 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '5a9aec6af1f59',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 3,
          'cell' => 2,
          'id' => 7,
          'widget_id' => '6b3ea9ff-81bc-43c9-a6d4-03b64e73cd57',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      8 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>
',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '20648579485adfb6ee3befa304917785',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 3,
          'cell' => 3,
          'id' => 8,
          'widget_id' => '38036252-eb3c-412b-a771-af976e03b944',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      9 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '5a9aec6b78b55',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 4,
          'cell' => 0,
          'id' => 9,
          'widget_id' => 'fc05a8fc-da62-4ccf-9557-b571f874af49',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      10 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '5a9aec6b334d1',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 4,
          'cell' => 1,
          'id' => 10,
          'widget_id' => 'd64fc1fd-3d90-4038-b22b-a498751d3a89',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      11 => 
      array (
        'title' => 'Služba',
        'image' => 39,
        'popup' => '<p>Obsah vyskakovacího okna</p>',
        'popup_selected_editor' => 'tinymce',
        '_sow_form_id' => '5a9aec7581b7d',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Reveal',
          'raw' => false,
          'grid' => 4,
          'cell' => 2,
          'id' => 11,
          'widget_id' => '93950fe7-c7c2-4819-b3a6-efc9072112fb',
          'style' => 
          array (
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      12 => 
      array (
        'pirate_forms_widget_title' => 'Napište nám!',
        'pirate_forms_widget_subtext' => 'Text nad formulářem',
        'panels_info' => 
        array (
          'class' => 'pirate_forms_contact_widget',
          'raw' => false,
          'grid' => 6,
          'cell' => 0,
          'id' => 12,
          'widget_id' => '4bc4994a-25d4-4e8a-8cf0-dfcde2143c43',
          'style' => 
          array (
            'widget_css' => 'front-contact-form',
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'bounceInLeft',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      13 => 
      array (
        'map' => '737',
        '_sow_form_id' => '4099113655ae9f648cddef640031928',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Mapycz',
          'raw' => false,
          'grid' => 6,
          'cell' => 1,
          'id' => 13,
          'widget_id' => 'efc8d74c-0564-40c7-b388-9a7930097d9a',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'bounceInRight',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      14 => 
      array (
        'title' => '',
        'text' => '<h2 style="text-align: center;">Řekli o nás</h2>
',
        'text_selected_editor' => 'tmce',
        'autop' => true,
        '_sow_form_id' => '5a97cb65f409a',
        '_sow_form_timestamp' => '1519897469402',
        'panels_info' => 
        array (
          'class' => 'SiteOrigin_Widget_Editor_Widget',
          'raw' => false,
          'grid' => 7,
          'cell' => 0,
          'id' => 14,
          'widget_id' => 'd8411d07-b367-4093-a680-b702404a9b41',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      15 => 
      array (
        'title' => 'Josef Novák',
        'company' => 'Firma Josefa Nováka, s.r.o.',
        'image' => 399,
        'testimony' => '<p>Vyjádření pana inženýra Josefa Nováka. Upravte podle skutečnosti.</p>
',
        'testimony_selected_editor' => 'tinymce',
        '_sow_form_id' => '5aa18f169cef3',
        '_sow_form_timestamp' => '1525282758266',
        'panels_info' => 
        array (
          'class' => 'Testimonial',
          'raw' => false,
          'grid' => 8,
          'cell' => 0,
          'id' => 15,
          'widget_id' => '78f5ce80-6c47-4bd3-bd32-1a3c0e768adf',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      16 => 
      array (
        'title' => 'Josef Novák',
        'company' => 'Firma Josefa Nováka, s.r.o.',
        'image' => 399,
        'testimony' => '<p>Vyjádření pana inženýra Josefa Nováka. Upravte podle skutečnosti.</p>
',
        'testimony_selected_editor' => 'tinymce',
        '_sow_form_id' => '11253163805ae9f7cda7e96341741711',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Testimonial',
          'raw' => false,
          'grid' => 8,
          'cell' => 1,
          'id' => 16,
          'widget_id' => '8275447f-5cc2-4e24-8d68-72a05bcdfcbe',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
      17 => 
      array (
        'title' => 'Josef Novák',
        'company' => 'Firma Josefa Nováka, s.r.o.',
        'image' => 399,
        'testimony' => '<p>Vyjádření pana inženýra Josefa Nováka. Upravte podle skutečnosti.</p>
',
        'testimony_selected_editor' => 'tinymce',
        '_sow_form_id' => '16085869995ae9f7cd39728658490380',
        '_sow_form_timestamp' => '',
        'panels_info' => 
        array (
          'class' => 'Testimonial',
          'raw' => false,
          'grid' => 8,
          'cell' => 2,
          'id' => 17,
          'widget_id' => '4084291f-5c01-49d3-ad58-ee052f0b9037',
          'style' => 
          array (
            'background_image_attachment' => false,
            'background_display' => 'tile',
            'animation_type' => 'fadeInUp',
            'animation_offset' => '10',
            'animation_iteration' => '1',
          ),
        ),
      ),
    ),
    'grids' => 
    array (
      0 => 
      array (
        'cells' => 4,
        'style' => 
        array (
          'id' => 'about',
          'padding' => '60px 0px 0px 0px',
          'mobile_padding' => '40px 0px 0px 0px',
          'background_image_attachment' => false,
          'background_display' => 'tile',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      1 => 
      array (
        'cells' => 1,
        'style' => 
        array (
          'row_css' => 'min-height:400px',
          'mobile_css' => 'min-height:300px',
          'background_image_attachment' => '278',
          'background_image_attachment_fallback' => false,
          'background_display' => 'parallax',
          'row_stretch' => 'full',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      2 => 
      array (
        'cells' => 1,
        'style' => 
        array (
          'id' => 'offer',
          'background_image_attachment' => false,
          'background_display' => 'tile',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      3 => 
      array (
        'cells' => 4,
        'style' => 
        array (
          'background_image_attachment' => false,
          'background_display' => 'tile',
          'cell_alignment' => 'stretch',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      4 => 
      array (
        'cells' => 3,
        'style' => 
        array (
          'background_display' => 'tile',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      5 => 
      array (
        'cells' => 1,
        'style' => 
        array (
          'row_css' => 'min-height:400px',
          'mobile_css' => 'min-height:300px',
          'background_image_attachment' => '279',
          'background_image_attachment_fallback' => false,
          'background_display' => 'parallax',
          'row_stretch' => 'full',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      6 => 
      array (
        'cells' => 2,
        'style' => 
        array (
          'id' => 'contact',
          'class' => 'diagonal-brand',
          'padding' => '30px 30px 30px 30px',
          'mobile_padding' => '15px 15px 15px 15px',
          'background_image_attachment' => false,
          'background_display' => 'tile',
          'bottom_margin' => '0px',
          'gutter' => '40px',
          'row_stretch' => 'full',
          'cell_alignment' => 'center',
        ),
      ),
      7 => 
      array (
        'cells' => 1,
        'style' => 
        array (
          'id' => 'references',
          'class' => 'bg-light-gray',
          'background_image_attachment' => false,
          'background_display' => 'tile',
          'row_stretch' => 'full',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
      8 => 
      array (
        'cells' => 3,
        'style' => 
        array (
          'class' => 'bg-light-gray',
          'background_image_attachment' => false,
          'background_image_attachment_fallback' => false,
          'background_display' => 'tile',
          'row_stretch' => 'full',
          'cell_alignment' => 'flex-start',
        ),
        'ratio' => 1,
        'ratio_direction' => 'right',
      ),
    ),
    'grid_cells' => 
    array (
      0 => 
      array (
        'grid' => 0,
        'index' => 0,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      1 => 
      array (
        'grid' => 0,
        'index' => 1,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      2 => 
      array (
        'grid' => 0,
        'index' => 2,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      3 => 
      array (
        'grid' => 0,
        'index' => 3,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      4 => 
      array (
        'grid' => 1,
        'index' => 0,
        'weight' => 1,
        'style' => 
        array (
        ),
      ),
      5 => 
      array (
        'grid' => 2,
        'index' => 0,
        'weight' => 1,
        'style' => 
        array (
        ),
      ),
      6 => 
      array (
        'grid' => 3,
        'index' => 0,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      7 => 
      array (
        'grid' => 3,
        'index' => 1,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      8 => 
      array (
        'grid' => 3,
        'index' => 2,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      9 => 
      array (
        'grid' => 3,
        'index' => 3,
        'weight' => 0.25,
        'style' => 
        array (
        ),
      ),
      10 => 
      array (
        'grid' => 4,
        'index' => 0,
        'weight' => 0.3333333333333333,
        'style' => 
        array (
        ),
      ),
      11 => 
      array (
        'grid' => 4,
        'index' => 1,
        'weight' => 0.3333333333333333,
        'style' => 
        array (
        ),
      ),
      12 => 
      array (
        'grid' => 4,
        'index' => 2,
        'weight' => 0.3333333333333333,
        'style' => 
        array (
        ),
      ),
      13 => 
      array (
        'grid' => 5,
        'index' => 0,
        'weight' => 1,
        'style' => 
        array (
        ),
      ),
      14 => 
      array (
        'grid' => 6,
        'index' => 0,
        'weight' => 0.5,
        'style' => 
        array (
        ),
      ),
      15 => 
      array (
        'grid' => 6,
        'index' => 1,
        'weight' => 0.5,
        'style' => 
        array (
        ),
      ),
      16 => 
      array (
        'grid' => 7,
        'index' => 0,
        'weight' => 1,
        'style' => 
        array (
        ),
      ),
      17 => 
      array (
        'grid' => 8,
        'index' => 0,
        'weight' => 0.3333333333333333,
        'style' => 
        array (
        ),
      ),
      18 => 
      array (
        'grid' => 8,
        'index' => 1,
        'weight' => 0.3333333333333333,
        'style' => 
        array (
        ),
      ),
      19 => 
      array (
        'grid' => 8,
        'index' => 2,
        'weight' => 0.3333333333333333,
        'style' => 
        array (
        ),
      ),
    ),
  );