<?php
/**
 * Start all functions of GrafiqueX
 * Big thanks to Reverie Theme - http://www.themefortress.com/reverie/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Start all the functions
add_action( 'after_setup_theme','bpf_init' );

if( !function_exists( 'bpf_init' ) ) {
	
	function bpf_init() {
	    
	    // Start head cleanup
	    add_action('init', 'bpf_head_cleanup');

	    // remove pesky injected css for recent comments widget
	    add_filter( 'wp_head', 'bpf_remove_wp_widget_recent_comments_style', 1 );	    
	    // clean up comment styles in the head
	    add_action('wp_head', 'bpf_remove_recent_comments_style', 1);

	    // remove WP version from RSS
	    add_filter('the_generator', 'bpf_rss_version');
	    
	    // clean up gallery output in wp
	    add_filter('gallery_style', 'bpf_gallery_style');

	    // enqueue base scripts and styles
	    add_action('wp_enqueue_scripts', 'bpf_scripts_and_styles', 999);
	    
	    // ie conditional wrapper
	    add_filter( 'style_loader_tag', 'bpf_ie_conditional', 10, 2 );
	    
	    // additional post related cleaning
	    add_filter( 'img_caption_shortcode', 'bpf_cleaner_caption', 10, 3 );
	    add_filter('get_image_tag_class', 'bpf_image_tag_class', 0, 4);
	    add_filter('get_image_tag', 'bpf_image_editor', 0, 4);
	    add_filter( 'the_content', 'bpf_img_unautop', 30 );

	    // Remove open sans font from head
	    add_action('wp_enqueue_scripts', 'remove_wp_open_sans');	
		// Comment below to unremove font from admin
		add_action('admin_enqueue_scripts', 'remove_wp_open_sans');

		// Remove and diable JSON API
		add_action( 'init', 'disable_json_api' );
		add_action( 'init', 'remove_json_api' );

	} // End of bpf_init() 
}

/**
 * Disable JSON API 
 */
function disable_json_api () {

  // Filters for WP-API version 1.x
  add_filter('json_enabled', '__return_false');
  add_filter('json_jsonp_enabled', '__return_false');

  // Filters for WP-API version 2.x
  add_filter('rest_enabled', '__return_false');
  add_filter('rest_jsonp_enabled', '__return_false');

}


/**
 * Remove JSON API 
 */
function remove_json_api () {

    // Remove the REST API lines from the HTML Header
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );

    // Remove the REST API endpoint.
    remove_action( 'rest_api_init', 'wp_oembed_register_route' );

    // Turn off oEmbed auto discovery.
    add_filter( 'embed_oembed_discover', '__return_false' );

    // Don't filter oEmbed results.
    remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

    // Remove oEmbed discovery links.
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );


}


/**
 * Remove CDN Open Sans font 
 */
if ( !function_exists( 'remove_wp_open_sans' ) ) {

	function remove_wp_open_sans() {
		wp_deregister_style( 'open-sans' );
		wp_register_style( 'open-sans', false );
	}
}

/**
 * Cleanup of WP_HEAD
 */
if( !function_exists( 'bpf_head_cleanup ' ) ) {
	
	function bpf_head_cleanup() {
		
		// category feeds
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		// post and comment feeds
		remove_action( 'wp_head', 'feed_links', 2 );
		// EditURI link
		remove_action( 'wp_head', 'rsd_link' );
		// windows live writer
		remove_action( 'wp_head', 'wlwmanifest_link' );
		// index link
		remove_action( 'wp_head', 'index_rel_link' );
		// previous link
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		// start link
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		// links for adjacent posts
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
		// WP version
		remove_action( 'wp_head', 'wp_generator' );
        // Shortlink in head        
	    remove_action('wp_head', 'wp_shortlink_wp_head'); 
	    // Removes emoji JS
	    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );  
	    // Removes emoji CSS
    	remove_action( 'wp_print_styles', 'print_emoji_styles' );
    	// Removes generator name from RSS
	    add_filter('the_generator', '__return_false');    
	    // Removes cookies
	    remove_action('set_comment_cookies', 'wp_set_comment_cookies');    

		// remove WP version from css
		add_filter( 'style_loader_src', 'bpf_remove_wp_ver_css_js', 9999 );
		// remove Wp version from scripts
		add_filter( 'script_loader_src', 'bpf_remove_wp_ver_css_js', 9999 );

	} // End of bpf_head_cleanup()
}


/**************************
    Cleaning functions
**************************/
/**
 * Remove WP version from RSS
 */
if( ! function_exists( 'bpf_rss_version ' ) ) {
	function bpf_rss_version() { return ''; }
}

/**
 * Remove WP version from scripts
 */
if( ! function_exists( 'bpf_remove_wp_ver_css_js ' ) ) {
	function bpf_remove_wp_ver_css_js( $src ) {
	    if ( strpos( $src, 'ver=' ) )
	        $src = remove_query_arg( 'ver', $src );
	    return $src;
	}
}

/**
 * Remove injected CSS for recent comments widget
 */
if( ! function_exists( 'bpf_remove_wp_widget_recent_comments_style ' ) ) {
	function bpf_remove_wp_widget_recent_comments_style() {
	   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
	      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
	   }
	}
}

/**
 * Remove injected CSS from recent comments widget
 */
if( ! function_exists( 'bpf_remove_recent_comments_style ' ) ) {
	function bpf_remove_recent_comments_style() {
	  global $wp_widget_factory;
	  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
	    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
	  }
	}
}

/**
 * Remove injected CSS from gallery
 */
if( ! function_exists( 'bpf_gallery_style ' ) ) {
	function bpf_gallery_style($css) {
	  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
	}
}



/*************************
  Post related cleaning
*************************/

/**
 * Custom output of captions
 */
if( ! function_exists( 'bpf_cleaner_caption ' ) ) {
	function bpf_cleaner_caption( $output, $attr, $content ) {

		/* We're not worried abut captions in feeds, so just return the output here. */
		if ( is_feed() )
			return $output;

		/* Set up the default arguments. */
		$defaults = array(
			'id' => '',
			'align' => 'alignnone',
			'width' => '',
			'caption' => ''
		);

		/* Merge the defaults with user input. */
		$attr = shortcode_atts( $defaults, $attr );

		/* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
		if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
			return $content;

		/* Set up the attributes for the caption <div>. */
		$attributes = ' class="figure ' . esc_attr( $attr['align'] ) . '"';

		/* Open the caption <div>. */
		$output = '<figure' . $attributes .'>';

		/* Allow shortcodes for the content the caption was created for. */
		$output .= do_shortcode( $content );

		/* Append the caption text. */
		$output .= '<figcaption>' . $attr['caption'] . '</figcaption>';

		/* Close the caption </div>. */
		$output .= '</figure>';

		/* Return the formatted, clean caption. */
		return $output;
		
	} /* End bpf_cleaner_caption */
}

// Clean the output of attributes of images in editor. Courtesy of SitePoint. http://www.sitepoint.com/wordpress-change-img-tag-html/
if( ! function_exists( 'bpf_image_tag_class ' ) ) {
	function bpf_image_tag_class($class, $id, $align, $size) {
		$align = 'align' . esc_attr($align);
		return $align;
	} /* end bpf_image_tag_class */
}

// Remove width and height in editor, for a better responsive world.
if( ! function_exists( 'bpf_image_editor ' ) ) {
	function bpf_image_editor($html, $id, $alt, $title) {
		return preg_replace(array(
				'/\s+width="\d+"/i',
				'/\s+height="\d+"/i',
				'/alt=""/i'
			),
			array(
				'',
				'',
				'',
				'alt="' . $title . '"'
			),
			$html);
	} /* end bpf_image_editor */
}

// Wrap images with figure tag. Courtesy of Interconnectit http://interconnectit.com/2175/how-to-remove-p-tags-from-images-in-wordpress/
if( ! function_exists( 'bpf_img_unautop ' ) ) {
	function bpf_img_unautop($pee) {
	    $pee = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<figure>$1</figure>', $pee);
	    return $pee;
	} /* end bpf_img_unautop */
}

?>
