<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

 /**
  * Načíst TGMPA knihovnu
  */
require_once( __DIR__. '/tgmpa/class-tgm-plugin-activation.php');

/**
 * Implementovat TGMPA knihovnu
 */
add_action( 'tgmpa_register', 'tgmpa_mixin_loader' );

// Pokud stejnojmenná funkce již existuje, nedělat nic
if (function_exists('tgmpa_mixin_loader')) {
    return;
}


function tgmpa_mixin_loader(){
    /**
     * Zcela na konci této funkce, za konfiguračními polemi
     * je nutné spustit funkci tgmpa($plugins,$config)
     */

    /**
     * Nastavení
     */

	$plugins = array(
		array(
			'name'      => 'CMB2 Framework',
			'slug'      => 'cmb2',
			'required'  => true,
			'force_activation'   => true,
		),
		array(
			'name'      => 'Disable comments',
			'slug'      => 'disable-comments',
			'required'  => true,
			'force_activation'   => true,
		),
		array(
			'name' => "Popups",
			"slug" => "popups",
			"required" => true,
			"force_activation" => true,
		),
		array(
			'name'      => 'Page Builder by SiteOrigin',
			'slug'      => 'siteorigin-panels',
			'required'  => true,
			'force_activation'   => true,
		),
		array(
			'name'      => 'SiteOrigin Widgets Bundle',
			'slug'      => 'so-widgets-bundle',
			'required'  => true,
			'force_activation'   => true,
		),
		array(
			'name'      => 'Duplicate Post',
			'slug'      => 'duplicate-post',
			'required'  => true,
			'force_activation'   => true,
		),
		array(
			'name'      => 'SO Page Builder Animate',
			'slug'      => 'so-page-builder-animate',
			'required'  => true,
			'force_activation'   => true,
		),
		array(
			'name'      => 'Pirate Forms',
			'slug'      => 'pirate-forms',
			'required'  => true,
			'force_activation'   => true,
		),
	);

    $config = array(
		'id'           => 'bpf',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'bpf-install-plugins', // Menu slug.
		'parent_slug'  => 'bpf-wizard',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => false,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		'strings'      => array(
			'page_title'                      => __( 'Required Plugins', 'bpf' ),
			'menu_title'                      => __( 'Required Plugins', 'bpf' ), 
			'installing'                      => __( 'Installing Plugin: %s', 'bpf' ),
			'updating'                        => __( 'Updating Plugin: %s', 'bpf' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'bpf' ),
			'notice_can_install_required'     => _n_noop(
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'bpf'
			),
			'notice_can_install_recommended'  => _n_noop(
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'bpf'
			),
			'notice_ask_to_update'            => _n_noop(
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'bpf'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'bpf'
			),
			'notice_can_activate_required'    => _n_noop(
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'bpf'
			),
			'notice_can_activate_recommended' => _n_noop(
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'bpf'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'bpf'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'bpf'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'bpf'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'bpf' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'bpf' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'bpf' ),
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'bpf' ),
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'bpf' ),
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'bpf' ),
			'dismiss'                         => __( 'Dismiss this notice', 'bpf' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'bpf' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'bpf' ),
			'nag_type'                        => '', 
		),
	);

	tgmpa( $plugins, $config );

}
