<?php

namespace MapyCZ;

defined ( 'ABSPATH' ) or die ( 'No script kiddies please!' );

class MapyCZ {

	function __construct() {
		//$inc_path   = plugin_dir_path ( __FILE__ ) . '/inc/';
		// $admin_path = plugin_dir_path ( __FILE__ ) . '/admin/';
		$inc_path = get_template_directory()."/lib/maps/inc/";
		$admin_path  = get_template_directory()."/lib/maps/admin/";

		$this->languages_path = basename ( dirname( __FILE__ ) ) . '/languages/';
		$this->inc_list       = array(
			'admin'    => $admin_path . 'admin.php',
			'front'    => $inc_path   . 'front.php',
			'map_ajax' => $inc_path   . 'map-ajax.php',
		);
	}

	public function init() {
		add_action ( 'init', array ( $this, 'register_map_post_type' ) );
		add_action ( 'admin_init', function() {
				include_once $this->inc_list['admin'];
				new Admin;
		});
		/*
		add_action ( 'plugins_loaded', function() {
			load_plugin_textdomain( 'mapy-cz', false, $this->languages_path );
		});
		*/

		add_shortcode ( 'mcz_map', function( $atts ) {
				include_once $this->inc_list['front'];
				if( ! isset( $this->front ) ) {
					$this->front = new Front;
				}
				return $this->front->mcz_map_shortcode( $atts );
		});

		add_action ( 'wp_ajax_mapy-cz-get-map', array ( $this, 'get_maps' ) );
		add_action ( 'wp_ajax_nopriv_mapy-cz-get-map', array ( $this, 'get_maps' ) );
	}

	function get_maps() {
		include_once $this->inc_list['map_ajax'];
		get_maps_ajax();
	}

	/**
	 * Registers a Custom Post Type: map
	*/
	function register_map_post_type() {
		register_post_type ( 'mapy-cz-map', array(
			'labels' => array(
				'name'               => __( 'Mapy.cz Maps', 'bpf' ),
				'singular_name'      => __( 'Mapy.cz Map', 'bpf' ),
				'menu_name'          => __( 'Mapy.cz', 'bpf' ),
				'name_admin_bar'     => __( 'Mapy.cz Map', 'bpf' ),
				'add_new'            => __( 'Add new', 'bpf' ),
				'add_new_item'       => __( 'Add new map', 'bpf' ),
				'new_item'           => __( 'New Map', 'bpf' ),
				'edit_item'          => __( 'Edit Map', 'bpf' ),
				'view_item'          => __( 'View Map', 'bpf' ),
				'all_items'          => __( 'All Maps', 'bpf' ),
				'search_items'       => __( 'Search Maps', 'bpf' ),
				'parent_item_colon'  => __( 'Parent Maps:', 'bpf' ),
				'not_found'          => __( 'No maps found.', 'bpf' ),
				'not_found_in_trash' => __( 'No maps found in Trash.', 'bpf' ),
			),

			// Frontend
			'has_archive'        => false,
			'public'             => false,
			'publicly_queryable' => false,

			// Admin
			'capability_type' => 'post',
			'show_ui'         => true,
			'menu_position'   => 30,
			'menu_icon'       => "dashicons-location-alt",
			'supports'        => array(
				'title',
				'author',
				'custom_fields',
				'revisions'
			),
		));
	}
}

$mapyCZ = new MapyCZ();
$mapyCZ->init();