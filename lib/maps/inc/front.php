<?php

namespace MapyCZ;

defined ( 'ABSPATH' ) or die ( 'No script kiddies please!' );

class Front {
	const APIURL = 'https://api4.mapy.cz/loader.js';
  
	/**
	 * [mcz_map id=<ID>]
	 */
	function mcz_map_shortcode( $atts ) {
		// Attributes
		extract ( shortcode_atts(
			array(
				'id' => null,
				'class' => '',
			),
			$atts
		));

		if ( is_null ( $id ) ) {
			return '<!-- map id unspecified -->';
		}

		//
		// TODO sanitize $id
		//
		$args = array(
			'post_type'  => 'mapy-cz-map',
			'meta_value' => $id,
		);
		$posts = get_posts ( $args );

		if ( isset( $posts[0] ) ) {
			if( $class != '' ) {
				$class .= ' ';
			}

			$class .= 'mcz-map mcz-map-' . $id;

			if ( isset ( $this->map_id_count[ $id ] ) ) {
				$this->map_id_count[$id]++;
				$id .= '-' . (string)$this->map_id_count[ $id ];
			}
			else {
				$this->map_id_count[ $id ] = 1;
			}

			$id = 'mcz-map-' . $id;
			$this->maps_to_load[ $id ] = $posts[0]->ID;

			if ( ! isset ( $this->loaded ) ) {
				$this->load();
			}

			return '<div id="' . esc_attr( $id ) . '" class="' . $class . '" tabindex="-1"></div>';
		}
		else {
			return '<!-- cannot find map with id: ' . $id . ' -->';
		}
	}
  
	function load() {
		wp_enqueue_style ( 'mcz-style', get_stylesheet_directory_uri(). '/lib/maps/css/mapy-cz.css');
		wp_enqueue_script ( 'mapy-cz-loader', self::APIURL, null, null, true);
		wp_enqueue_script ( 'mcz-script',get_stylesheet_directory_uri().'/lib/maps/js/mapy-cz.js' , array( 'jquery','mapy-cz-loader' ), null, true);
		$maps = array_keys($this->maps_to_load);;
		$mid = $this->maps_to_load[ $maps[0] ];
		$zoom = get_post_meta($mid,"_mapy-cz-map_zoom",true);
		if (empty($zoom)) {
			$zoom = 15;
		}
		wp_localize_script(
			'mcz-script',
			'mcz_map_ajax',
			array( 
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				"zoom" => $zoom,
			 )
		);

		add_action ( 'wp_footer', function() {
			wp_localize_script( 'mcz-script', 'mcz_load_maps', $this->maps_to_load );
		});

		$this->loaded = true;
	}
}