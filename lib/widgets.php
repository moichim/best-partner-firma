<?php
/**
 * Everything related to widgets
 * 
 * @package bpf
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}


$footer_cols = 0; # number of footer widgets

/**
 * Declare widget areas
 */
add_action( 'widgets_init', 'bpf_widgets_init' );
function bpf_widgets_init() {

    # United appearance of footer widgets
    global $footer_cols;
    $i = 1;
    while ( $i <= $footer_cols ) {
        register_sidebar( array(
            'name' => __( 'Footer '.$i, 'bpf' ),
            'id' => 'footer-'.$i,
            'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'bpf' ),
            'before_widget' => '<aside id="%1$s" class="widget footer-widget cell small-12 medium-6 large-4 %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h4 class="widget-title">',
            'after_title'   => '</h4>',
        ) );
        $i++;
    } # end declaration of footer widgets

    # here shall go global widgets

    register_sidebar( array(
        'name' => __( 'Posts sidebar', 'bpf' ),
        'id' => 'posts_sidebar',
        'description' => __( 'Widgets in this area will be shown in the entire blog-related pages.', 'bpf' ),
        'before_widget' => '<aside id="%1$s" class="widget sidebar-widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}


/**
 * Common declaration of footer sidebars
 */
function footer_sidebars() {
    global $footer_cols;
    $i = 1;
    while ( $i <= $footer_cols ) {
        $sidebar_slug = "footer-".$i;
        dynamic_sidebar($sidebar_slug);
        $i++;
    }
}

/**
 * Common declaration of posts sidebar
 */
function blog_sidebar() {
    print "<nav class='posts-sidebar'>";
    dynamic_sidebar("posts_sidebar");
    print "</nav>";
}


/**
 * Custom SiteOrigin Widgets
 */

function bpf_so_widgets($folders){
    $folders[] = get_template_directory() . "/lib/widgets/";
    return $folders;
}
add_filter('siteorigin_widgets_widget_folders', 'bpf_so_widgets');

/**
 * Purge the widget folder
 */
function bpf_remove_widgets($widgets){
    // unset($widgets['SiteOrigin_Widget_Button_Widget']);
    unset($widgets['WP_Widget_Tag_Cloud']);
    unset($widgets['WP_Widget_Recent_Comments']);
    // unset($widgets['WP_Widget_Recent_Posts']);
    unset($widgets['WP_Widget_Categories']);
    unset($widgets['WP_Widget_Calendar']);
    unset($widgets['WP_Widget_Meta']);
    // unset($widgets['WP_Widget_Archives']);
    unset($widgets['WP_Widget_Search']);
    unset($widgets['WP_Widget_Links']);
    unset($widgets['WP_Widget_Pages']);
    // unset($widgets['SiteOrigin_Panels_Widgets_PostLoop']);
    // unset($widgets['SiteOrigin_Panels_Widgets_Layout']);
    unset($widgets['SiteOrigin_Panels_Widgets_PostContent']);
    // unset($widgets['SiteOrigin_Widget_Image_Widget']);
    // unset($widgets['SiteOrigin_Widget_Video_Widget']);
    unset($widgets['SiteOrigin_Widgets_ContactForm_Widget']);
    // unset($widgets['SiteOrigin_Widget_Headline_Widget']);
    // unset($widgets['SiteOrigin_Widgets_ImageGrid_Widget']);
    unset($widgets['SiteOrigin_Widget_Simple_Masonry_Widget']);
    // unset($widgets['SiteOrigin_Widget_PriceTable_Widget']);
    // unset($widgets['SiteOrigin_Widget_Hero_Widget']);
    // unset($widgets['SiteOrigin_Widget_Cta_Widget']);
    // unset($widgets['SiteOrigin_Widget_Taxonomy_Widget']);
    unset($widgets['SiteOrigin_Widget_LayoutSlider_Widget']);
    // unset($widgets['SiteOrigin_Widget_Slider_Widget']);
    // unset($widgets['SiteOrigin_Widget_Tabs_Widget']);
    // unset($widgets['SiteOrigin_Widget_Features_Widget']);
    unset($widgets['SiteOrigin_Widgets_Testimonials_Widget']);
    return $widgets;
}
add_filter('siteorigin_panels_widgets', 'bpf_remove_widgets');


/**
 * Prebuilt layouts for SiteOrigin Panels
 */
function bpf_prebuilt_layouts($layouts){
    $layouts['row_3_reveals'] = array(
        // We'll add a title field
        'name' => __('Row: 3 services', 'bpf'), // Required
        # 'description' => __('Preset of image icons that expand into a popup window with details.', 'bpf'), // Optional
        'screenshot' => get_template_directory_uri( ) . '/lib/layouts/row-3-reveals/screenshot.jpg', // Optional
        'filename' => get_template_directory() . '/lib/layouts/row-3-reveals/data.json',
    );
    $layouts['row_3_counters'] = array(
        // We'll add a title field
        'name' => __('Row: 3 counters', 'bpf'), // Required
        # 'description' => __('Preset of numeric counters animated when visitor scrolls on them.', 'bpf'), # Optional
        'screenshot' => get_template_directory_uri( ) . '/lib/layouts/row-3-counters/screenshot.jpg', # Optional
        'filename' => get_template_directory() . '/lib/layouts/row-3-counters/data.json',
    );
    return $layouts;
}

add_filter( 'siteorigin_panels_prebuilt_layouts', 'bpf_prebuilt_layouts' );



/**
 * Adding magellan to widgets
 */
function custom_row_style_attributes( $attributes, $args ) {
    if ( !empty($args['id']) ) {
        $attributes["data-magellan-target"] = $args["id"];
    }
    return $attributes;
}

add_filter('siteorigin_panels_row_style_attributes', 'custom_row_style_attributes', 10, 2);