<?php 
/**
 * Definice shortcodů
 * 
 * @package bpf
 */
function bpf_person_form( $atts ) {
    global $post;
    if (gettype($post) == "object") {
        if (get_class($post) == "WP_Post") {
            $post_type = get_post_type();
            if (get_post_type($post)=="poradce") {
                $form = bpf_text_field($post->ID,"video_form");
                if ($form) {
                    $output = "<div class='form-styling'>";
                    $output .= $form;
                    $output .= "</div>";
                    return $output;
                }
            }
        }
    }
}
add_shortcode( 'person-form', 'bpf_person_form' );