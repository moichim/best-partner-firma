<?php
/**************************
 Handle CSS and JS scripts
**************************/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Enqueue CSS and JS scripts
 */
if( ! function_exists( 'bpf_scripts_and_styles ' ) ) {
	function bpf_scripts_and_styles() {
	  if (!is_admin()) {

		// = Something special for IE = //
		global $is_IE;
	    if ($is_IE) {
	       wp_register_script ( 'html5shiv', "http://html5shiv.googlecode.com/svn/trunk/html5.js" , false, true);
	    }

		// replace the WP jQuery with the version needed by Foundation
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', get_template_directory_uri() . "/assets/js/vendor/jquery.min.js" , false, true );

		// what-input
		# wp_register_script ( 'what-input', get_template_directory_uri() . "/assets/js/vendor/what-input.js" , false, true);

		// motion-ui
		wp_register_script ( 'motion-ui', get_template_directory_uri() . "/assets/js/vendor/motion-ui.js" , "jquery", true);

		// foundation.js
		wp_register_script ( 'foundation', get_template_directory_uri() . "/assets/js/foundation/foundation.min.js" , "jquery", true);

		// app.js
		wp_register_script ( 'bpf', get_template_directory_uri() . "/assets/js/app.js" , array("jquery","foundation","siteorigin-parallax"), true);
		
	    wp_enqueue_script( 'jquery' );	
	    wp_enqueue_script( 'html5shiv' );	
		# wp_enqueue_script( 'what-input' );	
		wp_enqueue_script( 'motion-ui' );	
		wp_enqueue_script( 'foundation' );
		wp_enqueue_script( 'bpf' );

	  	}
	}
}

/**
 * Add IE hack tag if needed
 */
if( ! function_exists( 'bpf_ie_conditional ' ) ) {
	function bpf_ie_conditional( $tag, $handle ) {
		if ( 'bpf-ie-only' == $handle )
			$tag = '<!--[if lt IE 9]>' . "\n" . $tag . '<![endif]-->' . "\n";
		return $tag;
	}
}

if( !function_exists( 'bpf_enqueue_style' ) ) {
	function bpf_enqueue_style() {

		# Check if a customized version of the foundation css file exists.
		$upload = wp_upload_dir();
		$customized = $upload["basedir"] . "/bpf-cache/app.css";
		if ( file_exists( $customized ) ) {
			$target = content_url() . "/uploads/bpf-cache/app.css";
			wp_register_style( 'foundation', $target, array(), '', 'all' );
		} 
		# If not, use the default  one.
		else {
			wp_register_style( 'foundation', get_template_directory_uri() . '/assets/css/app.css', array(), '', 'all' );
		}
	
		// Register style.css for WP purposes only
		wp_register_style( 'stylesheet', get_template_directory_uri() . '/style.css', array(), '', 'all' );

    	wp_enqueue_style( 'foundation' );
		# wp_enqueue_style( 'stylesheet' );
	}
}
add_action( 'wp_enqueue_scripts', 'bpf_enqueue_style' );


// Add admin CSS
function load_custom_wp_admin_style() {
    wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin.css', false);
    wp_enqueue_style( 'custom_wp_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );


?>
