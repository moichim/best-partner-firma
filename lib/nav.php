<?php 
/**
 * Definitions, helpers and overrides related to navigation
 * 
 * @package bpf
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/** ADMIN NAVIGATION */

/**
 * Moves Navigation link from Appearance to Navigation
 */
add_action('admin_menu', 'bpf_change_menus_position');
function bpf_change_menus_position() {
    # Remove the old menu
    remove_submenu_page( 'themes.php', 'nav-menus.php' );
    # Add a new menu page
     add_menu_page(
       __('Navigation','bpf'),
       __('Navigation','bpf'),
       'edit_theme_options',
       'nav-menus.php',
       '',
       'dashicons-list-view',
       3
    );
}

/** FRONTEND NAVIGATION */

/**
 * Menu positions
 */
add_action('after_setup_theme','bpf_menu_definitions');
if (!function_exists('bpf_menu_definitions')) {
	function bpf_menu_definitions(){
		register_nav_menus(array(
            'primary' => __('Main navigation',  'bpf'),
            'footer' => __('Footer', 'bpf'),
        )); 
	}
}

/** HELPERS */

/**
 * Hide empty taxonomies from menu
 */
add_filter( 'wp_get_nav_menu_items', 'bpf_remove_empty_terms', 10, 3 );
function bpf_remove_empty_terms ( $items, $menu, $args ) {
	global $wpdb;
	$empty = $wpdb->get_col( "SELECT term_taxonomy_id FROM $wpdb->term_taxonomy WHERE count = 0" );
	foreach ( $items as $key => $item ) {
		if ( ( 'taxonomy' == $item->type ) && ( in_array( $item->object_id, $empty ) ) ) {
			unset( $items[$key] );
		}
	}
	return $items;
}

/**
 * Adding custom class to menu items
 */
function bpf_custom_menu_item_classes( $classes, $item, $args ) {
	if( (is_tax('tax-portfolio-category') ) && 'Realizace' == $item->title )
		$classes[] = 'active';

	return array_unique( $classes );
}
add_filter( 'nav_menu_css_class', 'bpf_custom_menu_item_classes', 10, 3 );


