<?php
/**
 * Definice role Poradce
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

add_action('after_setup_theme','bpf_roles');

function bpf_roles(){
    $user = wp_get_current_user();
    // dev($user);
    $role = "final_poradce__xxxbdfa";
    $role_name = "Finální role pro poradce 55__f";
    remove_role($role);
    $poradce = get_role($role);
    # 1. seznam cílových rolí
    $poradce_allowed = array(
        "level_2",
        "read",
        "read_person",
        "read_private_people",
        "edit_person",
        "edit_people",
        "edit_published_people",
        "publish_people",
    );

    # Pokud Poradce neexistuje, vytvořit jej
    if (!$poradce) {
        add_role($role, $role_name, array());
        $poradce = get_role($role,true);
    }
    # zkontrolovat, zda má poradce patřičné role
    
    if (count($poradce->capabilities) > 0) {
        # polud poradce má nějaké role, zkontrolovat, zda má správné
        # zkontrolovat, jestli jsou role přes míru a případně je odebrat
        $over = array_diff($poradce_allowed,$poradce->capabilities);
        print_r($over);
        if (count($over)>0) {
            foreach ($over as $cap) {
                $poradce->remove_cap($cap);
            }
        }
        #zkontrolovat jsou-li role pod míru a případně je udělit
        $missing = array_diff($poradce->capabilities,$poradce_allowed);
        if (count($missing)>0) {
            foreach ($missing as $cap) {
                $poradce->add_cap($cap,true);
            }
        }
    } else {
        # pokud poradce nemá žádné role, prostě přidat ty správné
        foreach ($poradce_allowed as $cap) {
            $poradce->add_cap($cap);
        }
    }
    dev($poradce);
    ///dev(get_role("administrator"));

    # kontrola rolí u adminů
    # 1. definice rolí majících přístup k osobám
    $admins = array(get_role("editor"),get_role("administrator"));
    # 2. definice oprávnění adminů
    $admins_allowed = $poradce_allowed;
    array_push($admins_allowed,"delete_others_people");
    array_push($admins_allowed,"delete_published_people");
    array_push($admins_allowed,"delete_private_people");
    array_push($admins_allowed,"read_private_people"); array_push($admins_allowed,"edit_others_people");
    array_push($admins_allowed,"delete_people");     
    $missing = array(); # kontainer pro scházející oprávnění
    foreach ($admins as $admin) {
        # porovnám aktuální oprávnění role s povolenými a do $missing nasypu scházející
        $missing[$admin->name] = array_diff($admins_allowed, array_keys($admin->capabilities));
    }
    # pokud jsou scházející oprávnění, vložit je patřičným rolím
    if (count($missing)!=0) {
        $roles = array();
        foreach ($missing as $role => $caps) {
            $roler = get_role($role);
            foreach ($caps as $cap) {
                $roler->add_cap($cap);
            }
        }
    }
}