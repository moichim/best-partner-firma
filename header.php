<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width" />
    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
    <?php wp_head(); ?>
</head>

<body <?php body_class('antialiased'); ?>>

<!-- Off Canvas wrapper -->
<div class="off-canvas-wrapper">
    
    <?php 
    if (is_singular("poradce")) {
        get_template_part("template-parts/navigation/off-canvas-profile"); 
    } else {
        get_template_part("template-parts/navigation/off-canvas"); 
    }
    
    ?>

    <div class="off-canvas-wrapper-inner" data-sticky-container>

        <!-- Off Canvas content -->
        <div id="wrapper off-canvas-content" data-off-canvas-content>
            <!-- Top -->
            <div class="intro-wrapper" style="<?= bpf_featured_src(); ?>">