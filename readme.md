# BestPartner Firma
### Wordpress šablona pro weby Ziskových firem

-   autor: Jan Jáchim
-   použité technologie: [ZURB Foundation](http://foundation.zurb.com/sites/download.html/) 6.4.3., SCSS (compass)

### Instalace
1. Celou tuto šablonu stáhněte jako ZIP a nainstalujte ji do jakéhokoliv WordPressu. 
    -   Buďto přes webové rozhraní, kam se nahraje `ZIP`
    -   Anebo tím, že obsah tohoto repozitáře nahrajete přes FTP do složky `wp-content/themes/bpf`
2. Po instalaci a aktivaci šablony projděte celého *Průvodce nastavením*
3. Jakmile budou základní nastavení provedena, doporučujeme projít *Výchozí obsah*

### Doporučená nastavení Wordpressu, které šablona nemůže udělat za Vás
Dodatečná bezpečnostní posílení Wordpressu nejsou nutná pro běh webu ale velmi Vám usnadní práci a také web zabezpečí.
Na konec soboru `wp.config.php` přidejte tyto řádky:
```php
# USNADNĚNÍ: přímé nahrávání pluginů, šablon a pod.
define("FS_METHOD","direct");

# BEZPEČNOST: automatické aktualizace
define( 'WP_AUTO_UPDATE_CORE', true );

# BEZPEČNOST: zakázání editoru zdrojového kódu
define( 'DISALLOW_FILE_EDIT', true );
```

### Hlavní závislosti (info pro programátory)
- *SiteOrigin PageBuilder*, plugin pro budování homepage a layoutů na jiných místech webu
- *CMB2*, framework pro tvorbu vlastních polí
- *Pirate Forms*, plugin poskytující kontaktní formulář