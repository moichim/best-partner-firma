<?php
/**
 * BPF Starter Core
 * 
 * @package bpf
 */

/**
 * CONSTANTS
 */

# Theme version
define("THEME_VERSION", 0.9);

/**
 * Bootstrap & Load
 */

# TGMPA dependencies
require_once('lib/dependencies.php');

# Utility files
require_once('lib/utilities.php');

# Post type Person
require_once('lib/post-types/osoba.php');

# Page post type extensions
require_once('lib/post-types/page.php');

# Page post type extensions
require_once('lib/maps/mapy-cz.php');


# HTML head cleanup
require_once('lib/clean.php');

# Branding functionality
require_once('lib/branding.php');

# CSS and JS manager
require_once('lib/enqueue-style.php');

# Global template tags
require_once('lib/template-tags.php');

# Zurb Foundation related functionality
require_once('lib/foundation.php');

# Custom nav_menu walkers
require_once('lib/nav-walker.php');

# Menu definitions & functions
require_once('lib/nav.php');

# Widgets definitions & functions
require_once('lib/widgets.php');

# Settings module
require_once('lib/settings.php');

# Default content
require_once('lib/default-content.php');

# Custom user role
require_once('lib/roles.php');

# Include the necessary shortcode
require_once('lib/shortcodes.php');




/**
 * Register features for this theme
 */
if(!function_exists('bpf_template_support')) {
    function bpf_template_support() {
        load_theme_textdomain("bpf", get_template_directory() . '/lib/lang');  
        add_theme_support('post-thumbnails'); 
        set_post_thumbnail_size( 300, 300, true );
        # add_theme_support('automatic-feed-links');          
        add_theme_support('menus');
        add_theme_support('title-tag');
        
        # Custom header support & configuration
        $defaults = array(
            'default-image'          => '',
            'width'                  => 1500,
            'height'                 => 700,
            'flex-height'            => false,
            'flex-width'             => false,
            'uploads'                => true,
            'random-default'         => false,
            'header-text'            => false,
            'default-text-color'     => '',
            'wp-head-callback'       => '',
            'admin-head-callback'    => '',
            'admin-preview-callback' => '',
        );
        add_theme_support( 'custom-header', $defaults );
        # Custom logo
        add_theme_support( 'custom-logo', array(
            'height'      => 100,
            'width'       => 400,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        ) );
        # velikost obrázku
        add_image_size( 'square', 400, 400, array("center","center") ); // (cropped)
    }
}
add_action('after_setup_theme', 'bpf_template_support');

/**
 * Bezpečnostní nastavení
 */
if (defined("FS_METHOD")) {
    if (FS_METHOD == "direct") {
        # Pluginy
        add_filter( 'auto_update_plugin', '__return_true' );
        # Šablony
        add_filter( 'auto_update_theme', '__return_true' );
        # Jádro (Experimentální)
        add_filter( 'allow_minor_auto_core_updates', '__return_true' );
        add_filter( 'allow_major_auto_core_updates', '__return_true' ); 

    }
}

?>