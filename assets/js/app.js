/**
 * BPF application
 */
(function($) {
    
    // Code run once the document is fully loaded.
    $(document).ready(function(){

        $(document).foundation();
        
    }); // End $(document).ready();



     /** 
     * Funkce, která hlídá viditelnost věcí.
     * Převzato z respona. Tam sem to ukrad nevím od kud.
     */
    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();
        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();
        return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    /**
     * Trigger: vždy když se scrolluje
     */
    if ($("body").hasClass("home")) {
        $(window).scroll(function() {    
            if(isScrolledIntoView($('.count'))) {
                $('.count').each(function () {
                    if (!$(this).hasClass("finished")) {
                        $(this).addClass("finished");
                        
                        $(this).prop('Counter',0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        }, function(){
                            $(this).addClass("finished");                           
                        });
                    }
                });
            }    
        });
    }
    
    // Code run once the document is fully loaded.
    $(document).ready(function(){
        function isScrolledIntoView(elem) {
            if ($("body").hasClass("home")) {
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();
                var elemTop = $(elem).offset().top;
                var elemBottom = elemTop + $(elem).height();
                return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom) && (elemBottom <= docViewBottom) && (elemTop >= docViewTop));
            }
        }


        $(window).scroll(function() {    
            if(isScrolledIntoView($('.count'))){
                // alert('visible');
                $('.count').each(function () {
                    if (!$(this).hasClass("finished")) {
                        $(this).addClass("finished");
                        
                        $(this).prop('Counter',0).animate({
                            Counter: $(this).text()
                        }, {
                            duration: 1000,
                            easing: 'swing',
                            step: function (now) {
                                $(this).text(Math.ceil(now));
                            }
                        }, function(){
                            $(this).addClass("finished");                           
                        });
                    }
                });
            }    
        });
    });
	
})( jQuery );
