<?php get_header(); ?>

<?php get_template_part("template-parts/navigation/top-bar"); ?>

<?php if (have_posts()) : ?>

	<?php while (have_posts()): the_post(); ?>			
			
		<?php get_template_part('template-parts/posts/attachment','image'); ?>

	<?php endwhile; ?>

<?php endif; ?>
		
<?php get_footer(); ?>