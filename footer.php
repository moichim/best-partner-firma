                </main>
                <!-- The site footer -->
                <footer id="footer">
                    <section class="footer-top">
                        <div class="grid-container">
                            <div class="grid-x grid-padding-x grid-padding-y">
                                <div class="cell small-12">
                                    <a href="<?= site_url("/");?>">
                                        <strong class="site-name"><?= bloginfo("name"); ?></strong>
                                    </a>
                                    <span class="site-description"><?= bloginfo("description"); ?></span>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="footer-cols">
                        <div class="grid-container">
                            <div class="grid-x grid-padding-x grid-padding-y">
                            <nav class="primary-navigation show-for-large">
                                        <!-- Primary navigation -->
                                        <?php 
                                        wp_nav_menu( array(
                                            'theme_location'        => 'footer',
                                            'container'             => false,
                                            'depth'                 => 1,
                                            'items_wrap'            => '<ul class="dropdown menu" data-dropdown-menu data-options="closingTime:0; hoverDelay:0; alignment:left;">%3$s</ul>',
                                            'fallback_cb'           => 'bpf_menu_fallback', 
                                            'walker'                => new BPF_walker( 
                                                array(
                                                    'in_top_bar'            => true,
                                                    'item_type'             => 'li',
                                                    'menu_type'             => 'main-menu'
                                                ) 
                                            ),
                                        )); ?>
                                    </nav>
                                <?php # footer_sidebars(); ?>
                            </div>
                        </div>
                    </section>
                </footer>
            </div> <!-- /.off-canvas-wrapper-inner -->
        </div><!-- /.off-canvas-wrapper -->
        <!-- Scripts hooked to the end of the document -->
        <?php wp_footer(); ?>
    </body>
</html>