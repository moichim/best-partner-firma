<?php get_header(); ?>

<?php get_template_part("template-parts/navigation/top-bar"); ?>

		<div class="grid-x grid-padding-x column text-center page-heading">
			<div class="cell small-12 main-title archive-title pseudo-element text-center">
				<?php the_archive_title(); ?> 
			</div>
		</div>
	</div><!-- End of the top callout -->

	<!-- Main content wrapper --> 
	<main class="grid-container" role="main">
		<?php 
			$count = $wp_query->queried_object->category_count;
		?>
		<?php if($count > 3): ?>
		<!-- Archive pagination-->
		<div class="button-holder">
			<div class="button-lifter">
				<div class="button secondary static pagination">
					<?php bpf_pagination(); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<!-- Archive content -->
		<section class="grid-x grid-padding-x grid-padding-y ">
			<div class="cell small-12 medium-7 large-8 xlarge-9 global-margin-top">

				<?php if ( have_posts() ) : // If some posts available
					while ( have_posts() ) : the_post(); // Posts loop ?>
						
						<?php get_template_part("template-parts/posts/content","teaser"); ?>
					
					<?php endwhile; // End of posts loop
					else : // If no posts ?>

					<article id="post-0" class="post no-results not-found">
						<header>
							<h2><?php _e( 'Nothing Found', 'bpf' ); ?></h2>
						</header>
						<div class="entry-content">
							<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'bpf' ); ?></p>
						</div>
						<hr />
					</article>
					
				<?php endif; // End of posts if ?>
			</div>
			<div class="cell small-12 medium-5 large-4 xlarge-3 entry-content global-margin-top">
				<?php blog_sidebar(); ?>
			</div>

		</section>
		<?php if ($count > 3): ?>
		<!-- Archive pagination-->
		<div class="button-holder bottom">
			<div class="button-lifter">
				<div class="button secondary static pagination">
					<?php bpf_pagination(); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		
<?php get_footer(); ?>