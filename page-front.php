<?php /* Template Name: Úvodní stránka */ ?>

<?php get_header(); ?>

<?php get_template_part("template-parts/navigation/top-bar"); ?>

<?php if (have_posts()) : ?>

	<?php while (have_posts()): the_post(); ?>		
	<?php 
		/** Pokud je zapnutý DEBUG, na homepage se vyexportuje aktuální nastavení page buildera */
		if (WP_DEBUG==true) : 
	?>
	<pre>
		<?php 
			$data = get_post_meta($post->ID,"panels_data");
			var_export($data);
		?>
	</pre>
	<?php endif; ?>
        
    <?php get_template_part('template-parts/posts/content','front'); ?>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>
