<?php get_header(); ?>

<?php get_template_part("template-parts/navigation/top-bar"); ?>

		<div class="row column text-center page-heading">
			<div class="main-title index-title pseudo-element">
				<?php bloginfo("name"); ?> 
			</div>
		</div>
	</div><!-- End of the top callout -->
	
	<!-- Main content wrapper --> 
	<main class="container" role="main">

	<?php 
		$i = 0;
		if ( have_posts() ) {
			while (have_posts()) {
				the_post();
				get_template_part( 'template-parts/posts/content', "teaser" );	
			}
		}
		else {
			get_template_part( 'template-parts/posts/content', "none" );	
		}
	?>
		
<?php get_footer(); ?>
