<?php get_header(); ?>

<?php get_template_part("template-parts/navigation/top-bar"); ?>

		<div class="row column text-center introtext">
			<div class="main-title search-title pseudo-element">
            <?php _e('Search Results for', 'bpf'); ?> "<?php echo get_search_query(); ?>"
			</div>
		</div>
    </div><!-- End of the top callout -->
    
    <!-- Main content wrapper --> 
	<main class="container" role="main">

        <!-- Search pagination-->
        <div class="row column sub-line text-center listing">
            <?php bpf_pagination(); ?>
        </div>

        <!-- Search content -->
		<section class="row">
            <?php 
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        get_template_part("template-parts/content","teaser");
                    }
                } else {
                    print "Nothing found here";
                }
            ?>
        </search>
        
<?php get_footer(); ?>