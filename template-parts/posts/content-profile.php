<?php
/**
 * Template for unified rendering of formatted pages
 */
?>		

        <?php get_template_part("template-parts/navigation/top-bar-profile"); ?>
        <div class="grid-container">
            <div class="grid-x grid-padding-x page-heading" id="predstaveni" data-magellan-target="predstaveni">
                <div id="call" class="main-title profile-title pseudo-element cell small-12 medium-6 wow bounceInLeft grid-x">
                    <div class="cell small-12 vertical-center">
                        <?= bpf_text_field($post->ID,"person_call"); ?>
                    </div>
                </div>
                <div id="cropped-image" class="cell small-12 medium-6 wow bounceInRight text-center">
                    <?= bpf_image_field($post->ID,"person_image"); ?>
                </div>
            </div>
        </div>
	</div><!-- End of the top callout -->
    <?= bpf_embedded_video($post->ID); ?>
	
	<!-- Main content wrapper --> 
	<main role="main" class="global-margin-top">
        <article class="profile-<?php print $post->ID; ?>">
			<!-- The header of pages is output only because of SEO. It shall not be visible to any visitor, but hust be plyced inside of <article> element. -->
			<header class="page-header hide">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>
            <div class="grid-container">
                <!-- Řádka s představením -->
                <div class="section grid-x grid-padding-x grid-padding-y" id="o-mne" data-magellan-target="o-mne">
                    <div class="cell small-12 medium-6 main-portrait wow fadeInUp">
                        <?= bpf_image_field($post->ID,"person_full_portrait"); ?>
                    </div>
                    <div class="cell small-12 medium-6 main-portrait wow fadeInUp">
                        <?= bpf_text_field($post->ID,"person_presentation_text"); ?>
                    </div>
                </div>
                <!-- Řádka s nabídkou -->
                <div id="nabidka" data-magellan-target="nabidka" class="section grid-x grid-padding-x grid-padding-y">
                    <h2 class="cell small-12 text-center wow fadeInUp section-heading-2x"><?= bpf_text_field($post->ID,"person_services_introtext");?></h2>
                    <?php bpf_profile_offer(); ?>
                </div>
            </div>
            <!-- Separator image -->
            <div class="separator-image" style="background-image:url(<?= bpf_image_field_src($post->ID,"person_separator_1")?>);" data-siteorigin-parallax="{&quot;backgroundUrl&quot;:&quot;<?= bpf_image_field_src($post->ID,"person_separator_1")?>&quot;,&quot;backgroundSize&quot;:[2048,1367],&quot;backgroundSizing&quot;:&quot;scaled&quot;,&quot;limitMotion&quot;:&quot;auto&quot;}" data-stretch-type="full">
            </div>
            <!-- Řádka s kontaktem -->
            <div class="container">
                <div id="kontakt" data-magellan-target="kontakt" class="grid-x grid-padding-x grid-padding-y">
                    <div class="profile-contact cell small-12 large-5 text-center wow fadeInUp diagonal-brand">
                        <h2 class="section-heading">Kontakt</h2>
                        <?= esc_textarea( bpf_text_field($post->ID,"person_contact_text") );?>
                    </div>
                    <div class="cell small-12 large-7 text-center wow fadeInUp bg-light-gray global-padding">
                        <h2 class="section-heading">Blog</h2>
                        <?php bpf_profile_blog();?>
                    </div>
                </div>
            </div>
            <!-- Separator image -->
            <div class="separator-image" style="background-image:url(<?= bpf_image_field_src($post->ID,"person_separator_2")?>);" data-siteorigin-parallax="{&quot;backgroundUrl&quot;:&quot;<?= bpf_image_field_src($post->ID,"person_separator_2")?>&quot;,&quot;backgroundSize&quot;:[2048,1367],&quot;backgroundSizing&quot;:&quot;scaled&quot;,&quot;limitMotion&quot;:&quot;auto&quot;}" data-stretch-type="full">
            </div>

            <!-- Reference -->
            <div class="diagonal-light">
                <div class="grid-container">
                    <div id="reference" data-magellan-target="reference" class="grid-x grid-margin-x grid-padding-y">
                        <h2 class="cell small-12 text-center wow fadeInUp section-heading-2x"><?= bpf_text_field($post->ID,"person_references_introtext");?></h2>
                        <?php bpf_profile_testimonials(); ?>
                    </div>
                </div>
            </div>
		</article>	