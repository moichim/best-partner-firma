<?php
/**
 * Template for unified rendering of formatted pages
 */
?>		<div class="grid-x grid-padding-x text-center page-heading">
			<?php the_title( "<div class='cell small-12 main-title page-title pseudo-element'>", "</div>" ); ?> 
		</div>
	</div><!-- End of the top callout -->
	<?= bpf_embedded_video($post->ID); ?>
	
	<!-- Main content wrapper --> 
	<main class="grid-container" role="main">
		<article class="grid-x medium page-<?php print $post->ID; ?>">
			<!-- The header of pages is output only because of SEO. It shall not be visible to any visitor, but hust be plyced inside of <article> element. -->
			<header class="page-header small-12 cell align-center text-center hide">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>

			<div class="cell small-12 entry-content">
				<?php the_content(); ?>
			</div>
		</article>	