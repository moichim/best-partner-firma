<?php
/**
 * Template for unified rendering of formatted pages
 */
?>		<div class="grid-container">
			<?php if(!$image=bpf_heading_image($post)): ?>
			<!-- Stránka nemá náhledový obrázek -->
			<div class="grid-x column text-center page-heading">
				<?php the_title( "<div class='cell small-12 main-title page-title pseudo-element'>", "</div>" ); ?> 
			</div>
			<?php else: ?>
			<!-- Stránka má náhledový obrázek -->
			<div class="grid-x page-heading">
				<?php the_title( "<div id='call' class='cell small-12 medium-6 main-title vertical-center page-title pseudo-element wow bounceInLeft'>", "</div>" ); ?> 
				<div id="cropped-image" class="cell small-12 medium-6 text-center  wow bounceInRight">
					<img src="<?= $image; ?>">
				</div>
			</div>
			
			<?php endif; ?>
		</div>
	</div><!-- End of the top callout -->
	<?= bpf_embedded_video($post->ID); ?>
	
	<!-- Main content wrapper --> 
	<main class="grid-container" role="main">
		<article class="grid-x page-<?php print $post->ID; ?>">
			<!-- The header of pages is output only because of SEO. It shall not be visible to any visitor, but hust be plyced inside of <article> element. -->
			<header class="page-header small-12 cell text-center hide">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>

			<div class="cell small-12 entry-content">
				<?php the_content(); ?>
			</div>
		</article>	