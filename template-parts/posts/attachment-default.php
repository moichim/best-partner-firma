<?php
/**
 * Template for unified rendering of formatted pages
 */

 $id = bpf_get_attachment_post_id($post);
?>		<div class="grid-x grid-padding-x text-center page-heading">
			<div class="main-title attachment-title pseudo-element cell small-12">
				<?= bpf_get_attachment_post_title($post); ?>
			</div>
		</div>
	</div><!-- End of the top callout -->

	
	<!-- Main content wrapper --> 
	<main class="" role="main">
		<article class="grid-x grid-padding-x grid-padding-y diagonal-light attachment-<?php print $post->ID; ?>">
			<!-- The header of pages is output only because of SEO. It shall not be visible to any visitor, but hust be plyced inside of <article> element. -->
			<header class="page-header small-12 cell hide">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>

			<div class="cell small-12 medium-6 attachment-content no-gutter text-right full-gutter">
				<a href="<?= bpf_file_download_link($post);?>" target="_blank" class="button secondary">Stáhnout soubor</a>
				<?php if ($id): ?>
				<div class="button-lifter">
					<a href="<?= get_the_permalink($id); ?>" class="button secondary">Zpět na příspěvek <?= bpf_get_attachment_post_title($post); ?></a>
				</div>
				<?php endif; ?>
			</div>
			
			<div class="cell small-12 medium-6 full-gutter">
				<dl>
					<dt class="h5">Název souboru</dt>
					<dd><?= get_the_title(); ?></dd>
					<?php if ( has_excerpt() ) : ?>
					<dt class="h5">Popiska souboru</dt>
						<dd>
							<?= get_the_excerpt(); ?>
						</dd>
					<?php endif; ?>
					<dt class="h5">Podrobnosti o souboru</dt>
					<dd>
						<?= get_the_date(); ?>
						<?php if ($id) : ?>
							na stránce <a href="<?= the_permalink($id); ?>"><?= bpf_get_attachment_post_title(); ?></a>
						<?php endif; ?>
					</dd>
				</dl>
				
			</div>
		</article>	