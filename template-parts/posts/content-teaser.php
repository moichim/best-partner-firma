<?php
/**
 * Template for a global teaser.
 * 
 * @package bpf
 */
?>

<article class="post-teaser teaser-<?php print $post->ID; ?> wow fadeInUp">
    <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail("large");?>
        <header class="teaser-header">
            <h2 class="page-title"><?php the_title(); ?></h2>
        </header>
    </a>
    <div class="teaser-details">
        <div class="date"><?php the_date(); ?></div>
    </div>
    <div class="teaser-excerpt">
        <?php the_excerpt();?>
    </div>
    <footer>
        <a href="<?php the_permalink();?>" class="button secondary" rel="bookmark">Číst dále</a>
    </footer>
</article>