<?php
/**
 * Template for unified rendering of formatted pages
 */
?>

<?php get_template_part("template-parts/navigation/top-bar"); ?>

        <div class="row column text-center page-heading">
			<div class='main-title title-404 pseudo-element'>
                Stránka nenalezena <small>Na adrese, kterou jste zadali, se nám nepodařio nic najít.</small>
            </div>
            <p></p>
		</div>
	</div><!-- End of the top callout -->
	
	<!-- Main content wrapper --> 
	<main class="container" role="main">
		<article class="row medium page-<?php print $page->ID; ?>">
			<!-- The header of pages is output only because of SEO. It shall not be visible to any visitor, but hust be plyced inside of <article> element. -->
			<header class="page-header small-12 columns align-center text-center hide">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>

			<div class="columns small-12 entry-content">
				
			</div>
		</article>	