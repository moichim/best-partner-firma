<?php
/**
 * Template for unified rendering of formatted pages
 */

 $id = bpf_get_attachment_post_id($post);
?>		<div class="grid-x grid-padding-x column text-center page-heading">
			<div class="main-title attachment-title pseudo-element cell small-12">
				<?= bpf_get_attachment_post_title($post); ?>
			</div>
		</div>
	</div><!-- End of the top callout -->
	<?php if ($id): ?>
	<div class="button-holder">
		<div class="button-lifter">
			<a href="<?= get_the_permalink($id); ?>" class="button secondary">Zpět na příspěvek</a>
		</div>
	</div>
	<?php endif; ?>
	
	<!-- Main content wrapper --> 
	<main class="" role="main">
		<article class="grid-x attachment-<?php print $post->ID; ?>">
			<!-- The header of pages is output only because of SEO. It shall not be visible to any visitor, but hust be plyced inside of <article> element. -->
			<header class="page-header small-12 cell hide">
				<h1 class="page-title"><?php the_title(); ?></h1>
			</header>

			<div class="cell small-12 medium-7 attachment-content no-gutter text-right">
				<a href="<?= wp_get_attachment_image_url(get_the_ID(),'full'); ?>">
             		<?php echo wp_get_attachment_image( get_the_ID(), 'large' ); ?>
				</a>
				
			</div>
			<div class="cell small-12 medium-5 full-gutter">
				<dl>
					<dt class="h5">Název obrázku</dt>
					<dd><?= get_the_title(); ?></dd>
					<?php if ( has_excerpt() ) : ?>
					<dt class="h5">Popiska obrázku</dt>
						<dd>
							<?= get_the_excerpt(); ?>
						</dd>
					<?php endif; ?>
					<dt class="h5">Podrobnosti o obrázku</dt>
					<dd>
						Nahrán <?= get_the_date(); ?>
						<?php if ($id) : ?>
							<br>ke stránce <a href="<?= the_permalink($id); ?>"><?= bpf_get_attachment_post_title(); ?></a>
						<?php endif; ?>
					</dd>
				</dl>
				<a href="<?= bpf_image_download_link($post);?>" target="_blank" class="button secondary">Stáhnout obrázek</a>
			</div>
		</article>	