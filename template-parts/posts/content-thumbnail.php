<?php
/**
 * Template for a global teaser.
 * 
 * @package bpf
 */

 $thumb = get_the_post_thumbnail_url($post->ID,"large");
 $class = "thumb";
 if (!$thumb) {
    $logo_id = get_theme_mod( 'custom_logo' );
    $thumb = wp_get_attachment_image_url( $logo_id , 'full' );
    $class = "no-thumb";
 }
?>

<article class="post-thumbnail cell small-12 medium-6 thumbnail-<?= esc_attr( $post->ID ); ?> text-left">
    <a href="<?php the_permalink(); ?>">
        <div class="thumbnail-holder <?= $class; ?>" style="background-image:url(<?= esc_url( $thumb ); ?>);">
        </div>
        <header class="thumbnail-header global-padding">
            <h3 class="page-title"><?php the_title(); ?></h3>
        </header>
    </a>
    <div class="teaser-details global-padding">
        <div class="date color-medium-gray"><?php the_date(); ?></div>
    </div>
</article>