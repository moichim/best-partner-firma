<?php
/**
 * Template for unified rendering of posts details.
 * 
 * @package bpf
 */
?>

<?php
/**
 * Template for unified rendering of formatted pages
 */
?>
            <?php get_template_part("template-parts/navigation/top-bar"); ?>

        <div class="row column text-center page-heading">
            <div class="main-title post-title pseudo-element">
                <?php the_title(); ?>
            </div>
        </div>

    </div><!-- End of the top callout -->
    
    <div class="button-holder">
        <div class="button-lifter">
            <div class="button secondary">
                <span class="date">
                    <?php the_date(); ?>
                </span> |
                <span class="categories">
                    <?php print bpf_post_categories($post); ?>
                </span>
            </div>
        </div>
    </div>

	
	<!-- Main content wrapper --> 
	<main class="grid-container global-margin-top" role="main">
		<article class="grid-x grid-padding-x grid-padding-y post-<?= esc_attr( $post->ID ); ?>">
			<!-- The header is output only because of SEO -->
			<header class="page-header small-12 cell align-center text-center">
				<h1 class="page-title hide"><?php the_title(); ?></h1>
			</header>

			<div class="cell small-12 medium-7 large-8 xlarge-9 entry-content">
				<?php the_content(); ?>
			</div>
            <div class="cell small-12 medium-5 large-4 xlarge-3 entry-content">
				<?php blog_sidebar(); ?>
			</div>
		</article>	