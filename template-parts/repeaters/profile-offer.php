<?php 
    $item = get_query_var("offer"); 
    $count = get_query_var("offer_counter");
?>

<div class="cell small-12 medium-6 large-4 wow fadeInUp grow-1">
    <div class="reveal-widget" data-open="offer-<?= $count; ?>">
        <div class="reveal-image cursor-pointer" style="background-image:url(<?= esc_url( $item["image"] ); ?>);">
        </div>
        <div class="button-holder">
            <div class="button-lifter">
                <button class="button secondary reveal-button cursor-pointer">
                    <?= esc_textarea( $item["name"] ); ?>
                </button>
            </div>
        </div>
    </div>
</div>
<div id="offer-<?= $count; ?>" class="reveal" data-reveal data-animation-in="fade-in fast">
    <h2><?= esc_textarea( $item["name"] ); ?></h2>
    <?= esc_textarea( $item["description"] ); ?>
    <button class="close-button" data-close aria-label="Zavřít dialog" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

<?php set_query_var("offer_counter",$count+1);?>