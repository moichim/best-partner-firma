<?php $item = get_query_var("offer"); ?>
<div class="testimonial-item cell small-12 medium-4 wow fadeInUp no-gutter">
    <img src="<?= esc_url( $item["image"] ); ?>">
    <div class="global-padding">
        <h5><?= esc_textarea( $item["name"] ); ?>:<br><?= esc_textarea( $item["company"] ); ?></h5>
        <p><?= esc_textarea( $item["description"] ); ?></p>
    </div>
</div>