<?php 
/**
 * Template for displaying top bar in the header
 */
?>

        <!-- Top Bar -->
            <header class="header" data-sticky-container>
                <div class="top-bar-transformer" data-sticky data-margin-top="0" data-sticky-on="small">
                    <div class="top-bar grid-container">
                        <div class="top-bar-left grid-x">
                            <!-- Branding -->
                            <?php bpf_site_branding(); ?>
                            <nav class="primary-navigation show-for-large">
                                <!-- Primary navigation -->
                                <?php 
                                if ( is_front_page() || is_home() ) {
                                    $magellan = "data-magellan";
                                } else {
                                    $magellan = false;
                                }
                                wp_nav_menu( array(
                                    'theme_location'        => 'primary',
                                    'container'             => false,
                                    'depth'                 => 1,
                                    'items_wrap'            => '<ul class="dropdown menu" '. $magellan.' data-options="offset:50; treshold:100;">%3$s</ul>',
                                    'fallback_cb'           => 'bpf_menu_fallback', 
                                    'walker'                => new BPF_walker( 
                                        array(
                                            'in_top_bar'            => true,
                                            'item_type'             => 'li',
                                            'menu_type'             => 'main-menu'
                                        ) 
                                    ),
                                )); ?>
                            </nav>
                            <nav class="hide-for-large">
                                <button class="menu-toggler menu-icon" type="button" data-open="offCanvasPrimary"></button>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>