<?php
/**
 * Off canvas menu for profile pages
 */
?>

<!-- Off Canvas Content & Menu -->
<div class="off-canvas position-right" id="offCanvasPrimary" data-position="right" data-off-canvas="offCanvasPrimary">
    <div class="close-holder">
        <button class="close-button" aria-label="Zavřít navigaci" type="button" data-close="offCanvasPrimary">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <nav class="primary-navigation">
        <!-- Primary navigation -->
        <ul class="oc-menu vertical menu" data-magellan data-animation-duration="300" data-offset="150">
            <li  class="menu-item menu-item-main-menu menu-item-name top-level">
                <a href="#predstaveni" class="is-active"><?= get_the_title();?></a>
            </li>
            <li  class="menu-item menu-item-main-menu menu-item-o-mne top-level">
                <a  href="#o-mne">O mne</a>
            </li>
            <li  class="menu-item menu-item-main-menu menu-item-jak-to-delam top-level">
                <a  href="#nabidka">Jak to dělám</a>
            </li>
            <li  class="menu-item menu-item-main-menu menu-item-kontakt top-level">
                <a  href="#kontakt">Kontakt</a>
            </li>
            <li  class="menu-item menu-item-main-menu menu-item-reference top-level">
                <a  href="#reference">Reference</a>
            </li>
            <li  class="menu-item menu-item-main-menu menu-item-blog top-level">
                <a href="<?= bpf_default_category_url(); ?>">Blog</a>
            </li>
            <li  class="menu-item menu-item-main-menu menu-item-blog top-level">
            <?php bpf_site_branding(); ?>
            </li>
        </ul>
    </nav>
</div>