<?php
/**
 * Off canvas menu
 */
?>

<!-- Off Canvas Content & Menu -->
<div class="off-canvas position-right" id="offCanvasPrimary" data-position="right" data-off-canvas="offCanvasPrimary">
    <div class="close-holder">
        <button class="close-button" aria-label="Zavřít navigaci" type="button" data-close="offCanvasPrimary">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php // Primary topbar menu constructor  
        wp_nav_menu( array(
            'theme_location'        => 'primary',
            'container'             => false,
            'depth'                 => 1,
            'items_wrap'            => '<ul class="oc-menu vertical menu" data-magellan >%3$s</ul>',
            'fallback_cb'           => 'bpf_menu_fallback', // workaround to show a message to set up a menu
            'walker'                => new BPF_walker( 
                array(
                    'in_top_bar'            => true,
                    'item_type'             => 'li',
                    'menu_type'             => 'main-menu'
                ) 
            ),
        ) ); ?>
</div>