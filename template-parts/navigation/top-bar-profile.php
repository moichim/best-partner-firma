<?php 
/**
 * Template for displaying top bar in the header
 */
?>

                <!-- Top Bar -->
                    <header class="header" data-sticky-container>
                        <div class="top-bar-transformer" data-sticky data-margin-top="0" data-sticky-on="small">
                            <div class="top-bar grid-container">
                                <div class="top-bar-left grid-x">   
                                    <div class="hide-for-large" style="flex-grow:1">
                                        <ul class="dropdown menu">
                                            <li class="menu-item profile-topbar-name">
                                                <a href="<?=get_the_permalink();?>"><?php the_title(); ?></a>
                                            </li>
                                        </ul>
                                    </div>                     
                                    <nav class="primary-navigation show-for-large">
                                        <!-- Primary navigation -->
                                        <ul class="dropdown menu" data-magellan data-animation-duration="300" data-offset="150">
                                            <li  class="menu-item menu-item-main-menu menu-item-name top-level">
                                                <a href="#predstaveni" class="is-active"><?php the_title();?></a>
                                            </li>
                                            <li  class="menu-item menu-item-main-menu menu-item-o-mne top-level has-dropdown">
                                                <a  href="#o-mne">O mne</a>
                                            </li>
                                            <li  class="menu-item menu-item-main-menu menu-item-jak-to-delam top-level">
                                                <a  href="#nabidka">Jak to dělám</a>
                                            </li>
                                            <li  class="menu-item menu-item-main-menu menu-item-kontakt top-level">
                                                <a  href="#kontakt">Kontakt</a>
                                            </li>
                                            <li  class="menu-item menu-item-main-menu menu-item-reference top-level">
                                                <a  href="#reference">Reference</a>
                                            </li>
                                            <li  class="menu-item menu-item-main-menu menu-item-blog top-level">
                                                <a  href="<?= bpf_default_category_url();?>">Blog</a>
                                            </li>
                                        </ul>
                                    </nav>
                                    <nav class="hide-for-large">
                                        <button class="menu-toggler menu-icon" type="button" data-open="offCanvasPrimary"></button>
                                    </nav>
                                </div>
                                <div class="top-bar-right show-for-large">
                                    <!-- Branding -->
                                    <?php bpf_site_branding(); ?>
                                </div>
                            </div>
                        </div>
                    </header>