<?php 
    /** Načtení proměnné poslané z template-tags.php */
    $video = get_query_var("bpf_video");
?>
<div class="button-holder">
    <div class="button-lifter">
        <button type="button" id="embedded-video-trigger" class="button secondary"><?= $video["button"]; ?></button>
    </div>
</div>
<section id="embedded-video" class="off text-center">
        <!-- Kontejner pro video -->
            <div id="player" class="player frame">
                <div id="ytplayer"></div>
            </div>
            <div id="callback" class="callback form-styling frame off">
                <div class="callback-wrapper">
                    <div class="centering-wrapper">
                        <?= $video["text_after"]; ?>
                        <?php if ( $video["video_form"] ): ?>
                            <div class="callback-form-wrapper">
                                <?= $video["video_form"]; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
    <script id="embedded-video-with-callback">
        /** Globální proměnná pro přehrávač */
        var player;
        
        /** Event spouštěnící video kliknutím na button */
        $("#embedded-video-trigger").on("click",function(){
            $("#embedded-video").removeClass("off").addClass("on");
            // Asynchronní načtení videa a spuštění přehrávače
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/player_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        });

        /** Funkce spuštěná bezprostředně po stažení Youtube API */
        function onYouTubePlayerAPIReady() {
            player = new YT.Player('ytplayer', {
                height: '360',
                width: '640',
                videoId: '<?= $video["vid"]; ?>',
                autoplay: 1,
                events: {
                    'onReady': onPlayerReady, // autoplay
                    "onStateChange": videoFinished, // funkce spuštěná po konci videa
                },
                playerVars: { 
                    // 'controls': 0, 
                    'rel' : 0,
                    'fs' : 0,
                }
            });
        }
        /** Funkce řešící autoplay */
        function onPlayerReady(event) {
            event.target.playVideo();
        }
        /** Funkce spuštěná na konci videa */
        function videoFinished(event) {
            if (event.data == 0) {
                console.log("video skončilo");
                $("#player").addClass("off");
                $("#callback").removeClass("off");
            }
        }
    </script>
</section>